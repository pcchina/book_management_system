package com.llh.action;

import java.util.List;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.llh.dao.WordMethodDao;
import com.llh.dao.impl.WordMethodDaoImpl;
import com.llh.entity.AdminManagerInfo;
import com.llh.entity.NoteInfo;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

@ParentPackage("struts-default")
public class WordMethodAction extends ActionSupport {
	WordMethodDao wm = new WordMethodDaoImpl();
	/**
	 * 留言模块
	 */
	private static final long serialVersionUID = 1L;
	private int id;

	private String uname;

	private int noteId;

	private NoteInfo ni;

	private int pageCode;// 页码

	private int totalCode;// 总页数

	public String getUname() {
		return uname;
	}

	public void setUname(String uname) {
		this.uname = uname;
	}

	public int getNoteId() {
		return noteId;
	}

	public void setNoteId(int noteId) {
		this.noteId = noteId;
	}

	public int getPageCode() {
		return pageCode;
	}

	public void setPageCode(int pageCode) {
		this.pageCode = pageCode;
	}

	public int getTotalCode() {
		return totalCode;
	}

	public void setTotalCode(int totalCode) {
		this.totalCode = totalCode;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public NoteInfo getNi() {
		return ni;
	}

	public void setNi(NoteInfo ni) {
		this.ni = ni;
	}

	/**
	 * 添加留言信息
	 * 
	 * @return
	 */
	@Action(value = "addword", results = { @Result(name = "success", location = "/findmyselfword.action",type="redirectAction"),
			@Result(name = "input", location = "/error.jsp") })
	public String addWord() {
		wm.addWord(ni);
		return SUCCESS;
	}

	/**
	 * 查询自己所有的留言信息
	 * 
	 * @return
	 */
	@Action(value = "findmyselfword", results = { @Result(name = "success", location = "/showmyselfword.jsp") })
	public String findMyselfWord() {
		int pageSize = 7;
		AdminManagerInfo ami = (AdminManagerInfo) ServletActionContext.getRequest().getSession().getAttribute("am");
		int totalCount = wm.getCount(ami.getAmiId());
		if (totalCount % pageSize == 0) {
			totalCode = totalCount / pageSize;
		} else {
			totalCode = totalCount / pageSize + 1;
		}
		if (pageCode == 0) {
			pageCode = 1;
		} else {
			if (pageCode >= totalCode) {
				pageCode = totalCode;
			}
		}
		List<NoteInfo> nlist = wm.findSelfWord(ami.getAmiId(), pageCode, pageSize);
		ActionContext.getContext().getSession().put("nlist", nlist);
		return SUCCESS;
	}

	/**
	 * 查询自己一条留言的详情
	 * 
	 * @return
	 */
	@Action(value = "findNoteByid", results = { @Result(name = "success", location = "/shownotebyid.jsp") })
	public String findNoteByid() {
		NoteInfo ni = wm.findNoteByid(noteId);
		ActionContext.getContext().getSession().put("ni", ni);
		return SUCCESS;
	}

	/**
	 * 超管查看所有留言信息
	 * 
	 * @return
	 */
	@Action(value = "findAllNote", results = { @Result(name = "success", location = "/showallnote.jsp") })
	public String findAllNote() {
		int pageSize = 7;
		int totalCount = wm.getCount1();
		if (totalCount % pageSize == 0) {
			totalCode = totalCount / pageSize;
		} else {
			totalCode = totalCount / pageSize + 1;
		}
		if (pageCode == 0) {
			pageCode = 1;
		} else {
			if (pageCode >= totalCode) {
				pageCode = totalCode;
			}
		}
		List<NoteInfo> nlist = wm.findAllNote(pageCode, pageSize);
		ActionContext.getContext().getSession().put("nlist", nlist);
		return SUCCESS;
	}

	/**
	 * 超管查看所有留言
	 * 
	 * @return
	 */
	@Action(value = "findnotebyname", results = { @Result(name = "success", location = "/showallnote.jsp") })
	public String findNoteByname() {
		List<NoteInfo> nlist = wm.findNoteByname(uname);
		ActionContext.getContext().getSession().put("nlist", nlist);
		return SUCCESS;
	}
	/**
	 * 审核留言
	 * @return
	 */
	@Action(value = "checknote", results = { @Result(name = "success", location = "/checknote.jsp") })
	public String checkNote() {
		NoteInfo ni = wm.findNoteByid(noteId);
		ActionContext.getContext().getSession().put("ni", ni);
		return SUCCESS;
	}
	
	/**
	 * 审核留言通过
	 * @return
	 */
	@Action(value = "checkagree", results = { @Result(name = "success", location = "/findAllNote.action",type="redirectAction") })
	public String checkagreeNote() {
		wm.checkNote(id);
		return SUCCESS;
	}
	
	/**
	 * 删除留言
	 * @return
	 */
	@Action(value = "deletenote", results = { @Result(name = "success", location = "/findAllNote.action",type="redirectAction") })
	public String deleteNote() {
		wm.deleteNote(noteId);
		return SUCCESS;
	}
	
}
