package com.llh.action;

import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.llh.dao.BookMethodDao;
import com.llh.dao.impl.BookMethodDaoImpl;
import com.llh.entity.BookInfo;
import com.llh.entity.BookTypeInfo;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

@ParentPackage("struts-default")
public class BookMethodAction extends ActionSupport {

	/**
	 * 图书管理模块
	 */
	private static final long serialVersionUID = 1L;
	BookMethodDao bm = new BookMethodDaoImpl();
	private int id;

	private int pageCode;// 页码

	private int totalCode;// 总页数

	private BookInfo bif;

	private String ename;

	private int comId;

	public int getComId() {
		return comId;
	}

	public void setComId(int comId) {
		this.comId = comId;
	}

	public String getEname() {
		return ename;
	}

	public void setEname(String ename) {
		this.ename = ename;
	}

	public BookInfo getBif() {
		return bif;
	}

	public void setBif(BookInfo bif) {
		this.bif = bif;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getPageCode() {
		return pageCode;
	}

	public void setPageCode(int pageCode) {
		this.pageCode = pageCode;
	}

	public int getTotalCode() {
		return totalCode;
	}

	public void setTotalCode(int totalCode) {
		this.totalCode = totalCode;
	}

	/**
	 * 查询所有图书
	 * 
	 * @return
	 */
	@Action(value = "findAllBook", results = { @Result(name = "success", location = "/showbook.jsp") })
	public String findAllBook() {
		int pageSize = 8;
		int totalCount = bm.getCount();
		if (totalCount % pageSize == 0) {
			totalCode = totalCount / pageSize;
		} else {
			totalCode = totalCount / pageSize + 1;
		}
		if (pageCode == 0) {
			pageCode = 1;
		} else {
			if (pageCode >= totalCode) {
				pageCode = totalCode;
			}
		}
		List<BookInfo> blist = bm.findAllBook(pageCode, pageSize);
		List<BookInfo> bi = bm.findAllByName(ename);
		ActionContext.getContext().getSession().put("bi", bi);
		ActionContext.getContext().getSession().put("blist", blist);
		return SUCCESS;
	}

	/**
	 * 获取到要修改的图书信息
	 * 
	 * @return
	 */
	@Action(value = "updatebook", results = { @Result(name = "success", location = "/updatebook.jsp") })
	public String updateBook() {
		BookInfo bi = bm.findBookById(id);
		ActionContext.getContext().getSession().put("bi", bi);
		return SUCCESS;
	}

	/**
	 * 修改图书信息
	 * 
	 * @return
	 */
	@Action(value = "updatebyid", results = {
			@Result(name = "success", location = "/findAllBook.action", type = "redirectAction"),
			@Result(name = "input", location = "/error.jsp") })
	public String updateById() {
		BookInfo bin = bm.findBookById(bif.getBookId());
		// 剩余的数量=原有的总数量-原有的剩余数量
		int borrow = bin.getBookNumAll() - bin.getBookNum();
		int a = bm.updateById(bif);
		if (a != 0) {
			// 现在的剩余数量=修改的总数量-剩余的数量
			int bookNum = bif.getBookNumAll() - borrow;
			bm.update(bin, bookNum);
			return SUCCESS;
		} else {
			return INPUT;
		}
	}

	/**
	 * 通过id删除图书
	 * 
	 * @return
	 */
	@Action(value = "deletebook", results = {
			@Result(name = "success", location = "/findAllBook.action", type = "redirectAction") })
	public String deleteBook() {
		bm.deleteById(id);
		return SUCCESS;
	}

	/**
	 * 通过name进行模糊查询
	 * 
	 * @return
	 */
	@Action(value = "findAllByname", results = { @Result(name = "success", location = "/showbook.jsp") })
	public String findAllByName() {
		List<BookInfo> bi = bm.findAllByName(ename);
		ActionContext.getContext().getSession().put("bi", bi);
		return SUCCESS;
	}

	/**
	 * 查书的类型
	 * 
	 * @return
	 */
	@Action(value = "findBookType", results = { @Result(name = "success", location = "/addbook.jsp") })
	public String findBookType() {
		List<BookTypeInfo> bti = bm.findBookType();
		ActionContext.getContext().getSession().put("bti", bti);
		return SUCCESS;
	}

	/**
	 * 添加图书
	 * 
	 */
	@Action(value = "addbook", results = {
			@Result(name = "success", location = "/findAllBook.action", type = "redirectAction"),
			@Result(name = "input", location = "/error.jsp") })
	public String addBook() {
		int a = bm.addBook(bif);
		if (a == 0) {
			return INPUT;
		}
		return SUCCESS;
	}

	/**
	 * 推荐书管理
	 * 
	 * @return
	 */

	@Action(value = "commendBook", results = { @Result(name = "success", location = "/commendbook.jsp") })
	public String commendBook() {
		int pageSize = 8;
		int totalCount = bm.getCount();
		if (totalCount % pageSize == 0) {
			totalCode = totalCount / pageSize;
		} else {
			totalCode = totalCount / pageSize + 1;
		}
		if (pageCode == 0) {
			pageCode = 1;
		} else {
			if (pageCode >= totalCode) {
				pageCode = totalCode;
			}
		}
		List<BookInfo> blist = bm.findAllBook(pageCode, pageSize);
		ActionContext.getContext().getSession().put("blist", blist);
		return SUCCESS;
	}

	/**
	 * 推荐的图书与不推荐的图书，修改状态
	 * 
	 * @return
	 */
	@Action(value = "updatestate", results = {
			@Result(name = "success", location = "/commendBook.action", type = "redirectAction") })
	public String updateState() {
		int state = 1;
		bm.updateState(state, comId);
		return SUCCESS;
	}

	@Action(value = "updatestate1", results = {
			@Result(name = "success", location = "/commendBook.action", type = "redirectAction") })
	public String updateState1() {
		int state = 0;
		bm.updateState(state, comId);
		return SUCCESS;
	}

	/**
	 * 
	 * 显示所有推荐书
	 * 
	 * @return
	 */
	@Action(value = "showcomm", results = { @Result(name = "success", location = "/commendbook1.jsp") })
	public String showComm() {

		int pageSize = 8;
		int totalCount = bm.getCount1();
		if (totalCount % pageSize == 0) {
			totalCode = totalCount / pageSize;
		} else {
			totalCode = totalCount / pageSize + 1;
		}
		if (pageCode == 0) {
			pageCode = 1;
		} else {
			if (pageCode >= totalCode) {
				pageCode = totalCode;
			}
		}
		List<BookInfo> blist = bm.findAllComm(pageCode, pageSize);
		ActionContext.getContext().getSession().put("blist", blist);
		return SUCCESS;
	}

	/**
	 * 
	 * 显示所有借阅书
	 * 
	 * @return
	 */
	@Action(value = "borrowBook", results = { @Result(name = "success", location = "/borrowbook.jsp") })
	public String borrowBook() {
		int pageSize = 8;
		int totalCount = bm.getCount2();
		if (totalCount % pageSize == 0) {
			totalCode = totalCount / pageSize;
		} else {
			totalCode = totalCount / pageSize + 1;
		}
		if (pageCode == 0) {
			pageCode = 1;
		} else {
			if (pageCode >= totalCode) {
				pageCode = totalCode;
			}
		}
		List<BookInfo> blist = bm.findAllBorrow(pageCode, pageSize);
		ActionContext.getContext().getSession().put("blist", blist);
		return SUCCESS;
	}

	/**
	 * 清空借阅信息
	 * 
	 * @return
	 */
	@Action(value = "dropBorrow", results = {
			@Result(name = "success", location = "/borrowBook.action", type = "redirectAction") })
	public String dropBorrow() {
		BookInfo bio = bm.findBookById(comId);
		int count = bio.getBookNumAll();
		bm.dropBorrow(comId, count);
		return SUCCESS;
	}

}
