package com.llh.action;

import java.util.Date;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.llh.dao.LoginDao;
import com.llh.dao.impl.LoginDaoImpl;
import com.llh.entity.AdminManagerInfo;
import com.llh.entity.SuperManagerInfo;
import com.opensymphony.xwork2.ActionContext;

@ParentPackage("struts-default")
public class LoginAction {
	/**
	 * 登录模块
	 */

	private AdminManagerInfo ami;

	private SuperManagerInfo smi;

	private String pwd;

	private int id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public AdminManagerInfo getAmi() {
		return ami;
	}

	public void setAmi(AdminManagerInfo ami) {
		this.ami = ami;
	}

	public SuperManagerInfo getSmi() {
		return smi;
	}

	public void setSmi(SuperManagerInfo smi) {
		this.smi = smi;
	}

	LoginDao ld = new LoginDaoImpl();

	/**
	 * 普通管理员登录
	 * 
	 * @return
	 */
	public String AdminLogin() {
		AdminManagerInfo am = ld.adminLogin(ami.getAmiName(), ami.getAmiPwd());
		if (am != null) {
			Date d = new Date();
			ActionContext.getContext().getSession().put("d", d);
			ActionContext.getContext().getSession().put("am", am);
			return "success";
		}
		return "error";
	}

	/**
	 * 超级管理员登录
	 * 
	 * @return
	 */
	public String SuperLogin() {
		SuperManagerInfo sm = ld.superLogin(smi.getSmiName(), smi.getSmiPwd());
		if (sm != null) {
			Date d = new Date();
			ActionContext.getContext().getSession().put("d", d);
			ActionContext.getContext().getSession().put("sm", sm);
			return "success";
		}
		return "error";
	}

	/**
	 * 安全退出
	 * 
	 * @return
	 */
	@Action(value = "exit", results = { @Result(name = "suc", location = "/login.jsp") })
	public String exit() {
		System.out.println(ActionContext.getContext().getSession());
		ActionContext.getContext().getSession().clear();
		return "suc";
	}

	/**
	 * 超管查所有管理员
	 * 
	 * @return
	 */
	@Action(value = "manager", results = { @Result(name = "success", location = "/manager.jsp") })
	public String manager() {
		List<AdminManagerInfo> alist = ld.findAllAdmin();
		ActionContext.getContext().getSession().put("alist", alist);
		return "success";
	}

	/**
	 * 修改普通管理员密码
	 * 
	 * @return
	 */
	@Action(value = "updatepwd", results = {
			@Result(name = "success", location = "/manager.action", type = "redirectAction") })
	public String updatePwd() {
		ld.updatePwd(id, pwd);
		return "success";
	}

	/**
	 * 解除锁定
	 * 
	 * @return
	 */
	@Action(value = "unlock", results = {
			@Result(name = "success", location = "/manager.action", type = "redirectAction") })
	public String unlock() {
		ld.unlock(0, id);
		return "success";
	}

	/**
	 * 锁定账号
	 * 
	 * @return
	 */
	@Action(value = "lock", results = {
			@Result(name = "success", location = "/manager.action", type = "redirectAction") })
	public String lock() {
		ld.unlock(1, id);
		return "success";
	}

	/**
	 * 删除账号
	 * 
	 * @return
	 */
	@Action(value = "deleteadmin", results = {
			@Result(name = "success", location = "/manager.action", type = "redirectAction") })
	public String deleteAdmin() {
		ld.deleteNote(id);
		ld.delete(id);
		return "success";
	}
	
	/**
	 * 普通管理员修改密码
	 * @return
	 */
	@Action(value = "updatepassword", results = {
			@Result(name = "success", location = "/updatepwdsuccess.jsp") })
	public String updatePassword() {
		ld.updatePwd(id, pwd);
		return "success";
	}

}
