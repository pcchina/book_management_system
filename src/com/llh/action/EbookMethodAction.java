package com.llh.action;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.llh.dao.EbookMethodDao;
import com.llh.dao.impl.EbookMethodDaoImpl;
import com.llh.entity.EbookInfo;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

@ParentPackage("struts-default")
public class EbookMethodAction extends ActionSupport {

	/**
	 * 电子书管理模块
	 */
	EbookMethodDao md = new EbookMethodDaoImpl();
	private static final long serialVersionUID = 1L;

	private String fileName;

	private File doc[];

	private String docFileName[];

	private String ename;

	private int id;

	private int pageCode;// 页码

	private int totalCode;// 总页数

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getPageCode() {
		return pageCode;
	}

	public void setPageCode(int pageCode) {
		this.pageCode = pageCode;
	}

	public int getTotalCode() {
		return totalCode;
	}

	public void setTotalCode(int totalCode) {
		this.totalCode = totalCode;
	}

	public String getEname() {
		return ename;
	}

	public void setEname(String ename) {
		this.ename = ename;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public File[] getDoc() {
		return doc;
	}

	public void setDoc(File[] doc) {
		this.doc = doc;
	}

	public String[] getDocFileName() {
		return docFileName;
	}

	public void setDocFileName(String[] docFileName) {
		this.docFileName = docFileName;
	}

	/*
	 * 
	 * @see com.opensymphony.xwork2.ActionSupport#execute()
	 * 
	 * 电子书的上传下载
	 */
	@Action(value = "upload", interceptorRefs = { @InterceptorRef(value = "fileUpload", params = { "allowedExtensions",
			".txt,.docx,.jnt", "maximumSize", "2000000" }), @InterceptorRef(value = "defaultStack") }, results = {
					@Result(name = "success", location = "/uploadsuccess.jsp"),
					@Result(name = "input", location = "/error.jsp") })
	public String execute() {

		for (int i = 0; i < doc.length; i++) {
			File doc1 = doc[i];
			String docFileName1 = docFileName[i];
			String name = changeName(docFileName1);
			String path = ServletActionContext.getServletContext().getRealPath("/file/" + name);
			FileInputStream fis = null;
			FileOutputStream fos = null;
			try {
				fis = new FileInputStream(doc1);
				fos = new FileOutputStream(path);
				byte[] b = new byte[1024];
				int length = fis.read(b);
				while (length != -1) {
					fos.write(b, 0, length);
					length = fis.read(b);
				}
				// 上传至数据库
				md.upload(docFileName1, name, path);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				try {
					fis.close();
					fos.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return SUCCESS;
	}

	public String changeName(String oldName) {
		int index = oldName.indexOf(".");
		// String name = oldName.substring(0, index);书名
		String extension = oldName.substring(index, oldName.length());
		Date d = new Date();
		Random r = new Random();
		return d.getTime() + r.nextInt(9) + extension;
	}

	/**
	 * 通过书名查询电子图书
	 * 
	 * @return
	 */
	@Action(value = "findAllByename", results = { @Result(name = "success", location = "/showebook.jsp"),
			@Result(name = "input", location = "/findAll.action", type = "redirectAction") })
	public String findAllByname() {
		if (ename.length() != 0) {
			List<EbookInfo> ei = md.findAllByename(ename);
			ActionContext.getContext().getSession().put("ei", ei);
			return SUCCESS;
		} else {
			return INPUT;
		}
	}

	/**
	 * 删除电子书
	 * 
	 * @return
	 */
	@Action(value = "deleteebook", results = {
			@Result(name = "success", location = "/findAll.action", type = "redirectAction") })
	public String deleteEbookById() {
		System.out.println(id);
		md.deleteEbook(id);
		return SUCCESS;
	}

	/**
	 * 查询所有电子书
	 */
	@Action(value = "findAll", results = { @Result(name = "success", location = "/showebook.jsp") })
	public String find() {
		int pageSize = 8;
		int totalCount = md.getCount();
		if (totalCount % pageSize == 0) {
			totalCode = totalCount / pageSize;
		} else {
			totalCode = totalCount / pageSize + 1;
		}
		if (pageCode == 0) {
			pageCode = 1;
		} else {
			if (pageCode >= totalCode) {
				pageCode = totalCode;
			}
		}
		List<EbookInfo> elist = md.findAll(pageCode, pageSize);
		List<EbookInfo> ei = md.findAllByename(ename);
		ActionContext.getContext().getSession().put("ei", ei);
		ActionContext.getContext().getSession().put("elist", elist);
		return SUCCESS;
	}

}
