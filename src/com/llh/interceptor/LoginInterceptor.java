package com.llh.interceptor;

import java.util.Map;
import com.llh.action.LoginAction;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;

/**
 * 盗链接的拦截器
 * 
 * @author Administrator
 *
 */
public class LoginInterceptor extends AbstractInterceptor {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public String intercept(ActionInvocation invocation) throws Exception {
		// TODO Auto-generated method stub

		if (invocation.getAction().getClass() != LoginAction.class) {
			System.out.println("盗链接");
			Map<String, Object> session = ActionContext.getContext().getSession();
			if (session.get("adminUser") == null || session.get("superUser") == null) {
				return Action.LOGIN;
			}
		}
		return invocation.invoke();
	}

}
