package com.llh.entity;

public class BookInfo {

	private int bookId;

	private String bookName;

	private String bookAuthor;

	private int booktId;

	private String bookIsbn;

	private String bookIntroduce;

	private int bookPage;

	private String bookFontNumber;

	private String bookPublisher;

	private String bookIntime;

	private int bookNewbooks;

	private int bookState;
	
	private int bookNumAll;

	private int bookNum;
	
	private String bookTypeName;
	
	

	
	
	public String getBookTypeName() {
		return bookTypeName;
	}

	public void setBookTypeName(String bookTypeName) {
		this.bookTypeName = bookTypeName;
	}

	public int getBookNumAll() {
		return bookNumAll;
	}

	public void setBookNumAll(int bookNumAll) {
		this.bookNumAll = bookNumAll;
	}

	public int getBookId() {
		return bookId;
	}

	public void setBookId(int bookId) {
		this.bookId = bookId;
	}

	public String getBookName() {
		return bookName;
	}

	public void setBookName(String bookName) {
		this.bookName = bookName;
	}

	public String getBookAuthor() {
		return bookAuthor;
	}

	public void setBookAuthor(String bookAuthor) {
		this.bookAuthor = bookAuthor;
	}

	public int getBooktId() {
		return booktId;
	}

	public void setBooktId(int booktId) {
		this.booktId = booktId;
	}

	public String getBookIsbn() {
		return bookIsbn;
	}

	public void setBookIsbn(String bookIsbn) {
		this.bookIsbn = bookIsbn;
	}

	public String getBookIntroduce() {
		return bookIntroduce;
	}

	public void setBookIntroduce(String bookIntroduce) {
		this.bookIntroduce = bookIntroduce;
	}

	public int getBookPage() {
		return bookPage;
	}

	public void setBookPage(int bookPage) {
		this.bookPage = bookPage;
	}

	public String getBookFontNumber() {
		return bookFontNumber;
	}

	public void setBookFontNumber(String bookFontNumber) {
		this.bookFontNumber = bookFontNumber;
	}

	public String getBookPublisher() {
		return bookPublisher;
	}

	public void setBookPublisher(String bookPublisher) {
		this.bookPublisher = bookPublisher;
	}

	public String getBookIntime() {
		return bookIntime;
	}

	public void setBookIntime(String bookIntime) {
		this.bookIntime = bookIntime;
	}

	public int getBookNewbooks() {
		return bookNewbooks;
	}

	public void setBookNewbooks(int bookNewbooks) {
		this.bookNewbooks = bookNewbooks;
	}

	public int getBookState() {
		return bookState;
	}

	public void setBookState(int bookState) {
		this.bookState = bookState;
	}

	public int getBookNum() {
		return bookNum;
	}

	public void setBookNum(int bookNum) {
		this.bookNum = bookNum;
	}

	public BookInfo(int bookId, String bookName, String bookAuthor, int booktId, String bookIsbn, String bookIntroduce,
			int bookPage, String bookFontNumber, String bookPublisher, String bookIntime, int bookNewbooks,
			int bookState, int bookNum) {
		super();
		this.bookId = bookId;
		this.bookName = bookName;
		this.bookAuthor = bookAuthor;
		this.booktId = booktId;
		this.bookIsbn = bookIsbn;
		this.bookIntroduce = bookIntroduce;
		this.bookPage = bookPage;
		this.bookFontNumber = bookFontNumber;
		this.bookPublisher = bookPublisher;
		this.bookIntime = bookIntime;
		this.bookNewbooks = bookNewbooks;
		this.bookState = bookState;
		this.bookNum = bookNum;
	}
	
	public BookInfo(int bookId, String bookName, String bookAuthor, int booktId, String bookIsbn, String bookIntroduce,
			int bookPage, String bookFontNumber, String bookPublisher, String bookIntime, int bookNewbooks,
			int bookState, int bookNumAll, int bookNum) {
		super();
		this.bookId = bookId;
		this.bookName = bookName;
		this.bookAuthor = bookAuthor;
		this.booktId = booktId;
		this.bookIsbn = bookIsbn;
		this.bookIntroduce = bookIntroduce;
		this.bookPage = bookPage;
		this.bookFontNumber = bookFontNumber;
		this.bookPublisher = bookPublisher;
		this.bookIntime = bookIntime;
		this.bookNewbooks = bookNewbooks;
		this.bookState = bookState;
		this.bookNumAll = bookNumAll;
		this.bookNum = bookNum;
	}

	public BookInfo() {
		super();
	}

	public BookInfo(int bookId, String bookName, String bookAuthor, int booktId, String bookIsbn, String bookIntroduce,
			int bookPage, String bookFontNumber, String bookPublisher, String bookIntime, int bookNewbooks,
			int bookState, int bookNumAll, int bookNum, String bookTypeName) {
		super();
		this.bookId = bookId;
		this.bookName = bookName;
		this.bookAuthor = bookAuthor;
		this.booktId = booktId;
		this.bookIsbn = bookIsbn;
		this.bookIntroduce = bookIntroduce;
		this.bookPage = bookPage;
		this.bookFontNumber = bookFontNumber;
		this.bookPublisher = bookPublisher;
		this.bookIntime = bookIntime;
		this.bookNewbooks = bookNewbooks;
		this.bookState = bookState;
		this.bookNumAll = bookNumAll;
		this.bookNum = bookNum;
		this.bookTypeName = bookTypeName;
	}

	
}
