package com.llh.entity;

public class SuperManagerInfo {

	private int smiId;

	private String smiName;

	private String smiPwd;

	public int getSmiId() {
		return smiId;
	}

	public void setSmiId(int smiId) {
		this.smiId = smiId;
	}

	public String getSmiName() {
		return smiName;
	}

	public void setSmiName(String smiName) {
		this.smiName = smiName;
	}

	public String getSmiPwd() {
		return smiPwd;
	}

	public void setSmiPwd(String smiPwd) {
		this.smiPwd = smiPwd;
	}

	public SuperManagerInfo(int smiId, String smiName, String smiPwd) {
		super();
		this.smiId = smiId;
		this.smiName = smiName;
		this.smiPwd = smiPwd;
	}

	public SuperManagerInfo() {
		super();
	}

	public SuperManagerInfo(String smiName, String smiPwd) {
		super();
		this.smiName = smiName;
		this.smiPwd = smiPwd;
	}

}
