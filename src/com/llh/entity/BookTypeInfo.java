package com.llh.entity;

public class BookTypeInfo {

	private int btiId;

	private String btiName;

	public int getBtiId() {
		return btiId;
	}

	public void setBtiId(int btiId) {
		this.btiId = btiId;
	}

	public String getBtiName() {
		return btiName;
	}

	public void setBtiName(String btiName) {
		this.btiName = btiName;
	}

	public BookTypeInfo(int btiId, String btiName) {
		super();
		this.btiId = btiId;
		this.btiName = btiName;
	}

	public BookTypeInfo() {
		super();
	}

	public BookTypeInfo(String btiName) {
		super();
		this.btiName = btiName;
	}

}
