package com.llh.entity;

public class NoteInfo {

	private int noteId;

	private int adimnId;

	private String noteTitle;

	private String noteAuthor;

	private String noteContent;

	private String noteTime;

	private int noteState;

	public int getAdimnId() {
		return adimnId;
	}

	public void setAdimnId(int adimnId) {
		this.adimnId = adimnId;
	}

	public int getNoteId() {
		return noteId;
	}

	public void setNoteId(int noteId) {
		this.noteId = noteId;
	}

	public String getNoteTitle() {
		return noteTitle;
	}

	public void setNoteTitle(String noteTitle) {
		this.noteTitle = noteTitle;
	}

	public String getNoteAuthor() {
		return noteAuthor;
	}

	public void setNoteAuthor(String noteAuthor) {
		this.noteAuthor = noteAuthor;
	}

	public String getNoteContent() {
		return noteContent;
	}

	public void setNoteContent(String noteContent) {
		this.noteContent = noteContent;
	}

	public String getNoteTime() {
		return noteTime;
	}

	public void setNoteTime(String noteTime) {
		this.noteTime = noteTime;
	}

	public int getNoteState() {
		return noteState;
	}

	public void setNoteState(int noteState) {
		this.noteState = noteState;
	}

	public NoteInfo(int noteId, String noteTitle, String noteAuthor, String noteContent, String noteTime) {
		super();
		this.noteId = noteId;
		this.noteTitle = noteTitle;
		this.noteAuthor = noteAuthor;
		this.noteContent = noteContent;
		this.noteTime = noteTime;
	}

	public NoteInfo() {
		super();
	}

	public NoteInfo(int noteId, String noteTitle, String noteAuthor, String noteContent, String noteTime,
			int noteState) {
		super();
		this.noteId = noteId;
		this.noteTitle = noteTitle;
		this.noteAuthor = noteAuthor;
		this.noteContent = noteContent;
		this.noteTime = noteTime;
		this.noteState = noteState;
	}

	public NoteInfo(int noteId, int adimnId, String noteTitle, String noteAuthor, String noteContent, String noteTime,
			int noteState) {
		super();
		this.noteId = noteId;
		this.adimnId = adimnId;
		this.noteTitle = noteTitle;
		this.noteAuthor = noteAuthor;
		this.noteContent = noteContent;
		this.noteTime = noteTime;
		this.noteState = noteState;
	}

}
