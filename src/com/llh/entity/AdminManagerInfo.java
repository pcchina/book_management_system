package com.llh.entity;

public class AdminManagerInfo {

	private int amiId;

	private String amiName;

	private String amiPwd;

	private int amiState;

	public int getAmiId() {
		return amiId;
	}

	public void setAmiId(int amiId) {
		this.amiId = amiId;
	}

	public String getAmiName() {
		return amiName;
	}

	public void setAmiName(String amiName) {
		this.amiName = amiName;
	}

	public String getAmiPwd() {
		return amiPwd;
	}

	public void setAmiPwd(String amiPwd) {
		this.amiPwd = amiPwd;
	}

	public int getAmiState() {
		return amiState;
	}

	public void setAmiState(int amiState) {
		this.amiState = amiState;
	}

	public AdminManagerInfo(int amiId, String amiName, String amiPwd, int amiState) {
		super();
		this.amiId = amiId;
		this.amiName = amiName;
		this.amiPwd = amiPwd;
		this.amiState = amiState;
	}

	public AdminManagerInfo() {
		super();
	}

	public AdminManagerInfo(String amiName, String amiPwd, int amiState) {
		super();
		this.amiName = amiName;
		this.amiPwd = amiPwd;
		this.amiState = amiState;
	}
}
