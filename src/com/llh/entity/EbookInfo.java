package com.llh.entity;

public class EbookInfo {

	private int eiId;

	private String eiName;
	
	private String eiFileName;

	private String eiTime;
	
	private int eiCount;

	private String eiSize;
	
	

	public String getEiFileName() {
		return eiFileName;
	}

	public void setEiFileName(String eiFileName) {
		this.eiFileName = eiFileName;
	}

	public int getEiCount() {
		return eiCount;
	}

	public void setEiCount(int eiCount) {
		this.eiCount = eiCount;
	}

	public int getEiId() {
		return eiId;
	}

	public void setEiId(int eiId) {
		this.eiId = eiId;
	}

	public String getEiName() {
		return eiName;
	}

	public void setEiName(String eiName) {
		this.eiName = eiName;
	}

	public String getEiTime() {
		return eiTime;
	}

	public void setEiTime(String eiTime) {
		this.eiTime = eiTime;
	}

	public String getEiSize() {
		return eiSize;
	}

	public void setEiSize(String eiSize) {
		this.eiSize = eiSize;
	}


	public EbookInfo(int eiId, String eiName, String eiFileName, String eiTime, int eiCount, String eiSize) {
		super();
		this.eiId = eiId;
		this.eiName = eiName;
		this.eiFileName = eiFileName;
		this.eiTime = eiTime;
		this.eiCount = eiCount;
		this.eiSize = eiSize;
	}

	public EbookInfo() {
		super();
	}

}
