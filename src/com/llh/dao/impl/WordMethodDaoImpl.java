package com.llh.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.llh.dao.WordMethodDao;
import com.llh.entity.NoteInfo;
import com.llh.util.DBHelper;

public class WordMethodDaoImpl implements WordMethodDao {
	PreparedStatement ps = null;
	ResultSet rs = null;
	Connection conn = null;

	@Override
	public int addWord(NoteInfo ni) {
		// TODO Auto-generated method stub
		int a = 0;
		conn = DBHelper.getCon();
		String sql = "insert into note(am_id,n_title,n_author,n_content,n_time) values(" + ni.getAdimnId() + ",'"
				+ ni.getNoteTitle() + "','" + ni.getNoteAuthor() + "','" + ni.getNoteContent() + "',now())";
		try {
			ps = conn.prepareStatement(sql);
			a = ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBHelper.realease(conn);
			DBHelper.closePreparedStatement(ps);
		}
		return a;
	}

	@Override
	public List<NoteInfo> findSelfWord(int id, int pageCode, int pageSize) {
		// TODO Auto-generated method stub
		List<NoteInfo> nlist = new ArrayList<NoteInfo>();
		NoteInfo ni = null;
		conn = DBHelper.getCon();
		int a = pageSize * (pageCode - 1);
		String sql = "select * from note where am_id=" + id + " and n_id limit " + a + "," + pageSize + "";
		try {
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				ni = new NoteInfo();
				ni.setNoteId(rs.getInt(1));
				ni.setAdimnId(rs.getInt(2));
				ni.setNoteTitle(rs.getString(3));
				ni.setNoteAuthor(rs.getString(4));
				ni.setNoteContent(rs.getString(5));
				ni.setNoteTime(rs.getString(6));
				ni.setNoteState(rs.getInt(7));
				nlist.add(ni);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return nlist;
	}

	@Override
	public int getCount(int id) {
		// TODO Auto-generated method stub
		int a = 0;
		conn = DBHelper.getCon();
		String sql = "select COUNT(*) from note where am_id=" + id + "";
		try {
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				a = rs.getInt(1);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBHelper.realease(conn);
			DBHelper.closePreparedStatement(ps);
			DBHelper.closeResultSet(rs);
		}

		return a;
	}

	@Override
	public NoteInfo findNoteByid(int id) {
		// TODO Auto-generated method stub
		NoteInfo ni = null;
		conn = DBHelper.getCon();
		String sql = "select * from note where n_id = " + id + "";
		try {
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				ni = new NoteInfo();
				ni.setNoteId(rs.getInt(1));
				ni.setAdimnId(rs.getInt(2));
				ni.setNoteTitle(rs.getString(3));
				ni.setNoteAuthor(rs.getString(4));
				ni.setNoteContent(rs.getString(5));
				ni.setNoteTime(rs.getString(6));
				ni.setNoteState(rs.getInt(7));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBHelper.realease(conn);
			DBHelper.closePreparedStatement(ps);
			DBHelper.closeResultSet(rs);
		}
		return ni;
	}

	@Override
	public int getCount1() {
		// TODO Auto-generated method stub
		int a = 0;
		conn = DBHelper.getCon();
		String sql = "select COUNT(*) from note";
		try {
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				a = rs.getInt(1);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBHelper.realease(conn);
			DBHelper.closePreparedStatement(ps);
			DBHelper.closeResultSet(rs);
		}

		return a;
	}

	@Override
	public List<NoteInfo> findAllNote(int pageCode, int pageSize) {
		// TODO Auto-generated method stub
		List<NoteInfo> nlist = new ArrayList<NoteInfo>();
		NoteInfo ni = null;
		conn = DBHelper.getCon();
		int a = pageSize * (pageCode - 1);
		String sql = "select * from(select * from note order by n_time desc) as n_id where n_id limit " + a + ","
				+ pageSize + "";
		try {
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				ni = new NoteInfo();
				ni.setNoteId(rs.getInt(1));
				ni.setAdimnId(rs.getInt(2));
				ni.setNoteTitle(rs.getString(3));
				ni.setNoteAuthor(rs.getString(4));
				ni.setNoteContent(rs.getString(5));
				ni.setNoteTime(rs.getString(6));
				ni.setNoteState(rs.getInt(7));
				nlist.add(ni);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBHelper.realease(conn);
			DBHelper.closePreparedStatement(ps);
			DBHelper.closeResultSet(rs);
		}
		return nlist;
	}

	@Override
	public List<NoteInfo> findNoteByname(String name) {
		// TODO Auto-generated method stub
		conn = DBHelper.getCon();
		NoteInfo ni = null;
		List<NoteInfo> nlist = new ArrayList<NoteInfo>();
		String sql = "select * from note where n_author like '%" + name + "%'";
		try {
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				ni = new NoteInfo();
				ni.setNoteId(rs.getInt(1));
				ni.setAdimnId(rs.getInt(2));
				ni.setNoteTitle(rs.getString(3));
				ni.setNoteAuthor(rs.getString(4));
				ni.setNoteContent(rs.getString(5));
				ni.setNoteTime(rs.getString(6));
				ni.setNoteState(rs.getInt(7));
				nlist.add(ni);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBHelper.realease(conn);
			DBHelper.closePreparedStatement(ps);
			DBHelper.closeResultSet(rs);
		}
		return nlist;
	}

	@Override
	public int checkNote(int id) {
		// TODO Auto-generated method stub
		int a = 0;
		conn = DBHelper.getCon();
		String sql = "update note set n_state=1 where n_id="+id;
		try {
			ps = conn.prepareStatement(sql);
			a = ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBHelper.realease(conn);
			DBHelper.closePreparedStatement(ps);
		}
		return a;
	}

	@Override
	public int deleteNote(int id) {
		// TODO Auto-generated method stub
		int a = 0;
		conn = DBHelper.getCon();
		String sql = "delete from note where n_id=" + id;
		try {
			ps = conn.prepareStatement(sql);
			a = ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBHelper.realease(conn);
			DBHelper.closePreparedStatement(ps);
		}
		return a;
	}

}
