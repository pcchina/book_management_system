package com.llh.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.llh.dao.EbookMethodDao;
import com.llh.entity.EbookInfo;
import com.llh.util.DBHelper;

public class EbookMethodDaoImpl implements EbookMethodDao {
	Connection conn = null;
	PreparedStatement ps = null;
	ResultSet rs = null;

	@Override
	public int upload(String ename, String efileName, String path) {
		// TODO Auto-generated method stub
		int a = 0;
		conn = DBHelper.getCon();
		String sql = "insert into ebook(e_name,e_filename,e_time,e_size) values('" + ename + "','" + efileName
				+ "',now(),'" + path + "')";
		try {
			ps = conn.prepareStatement(sql);
			a = ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBHelper.realease(conn);
			DBHelper.closePreparedStatement(ps);
		}
		return a;
	}

	@Override
	public List<EbookInfo> findAll(int pageCode,int pageSize) {
		// TODO Auto-generated method stub
		List<EbookInfo> elist = new ArrayList<EbookInfo>();
		conn = DBHelper.getCon();
		int a = pageSize*(pageCode-1)+1;
		String sql = "select * from ebook where e_id between "+a+" and "+pageSize*pageCode+"";
		try {
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				EbookInfo ei = new EbookInfo();
				ei.setEiId(rs.getInt(1));
				ei.setEiName(rs.getString(2));
				ei.setEiFileName(rs.getString(3));
				ei.setEiTime(rs.getString(4));
				ei.setEiCount(rs.getInt(5));
				ei.setEiSize(rs.getString(6));
				elist.add(ei);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBHelper.realease(conn);
			DBHelper.closePreparedStatement(ps);
			DBHelper.closeResultSet(rs);
		}
		return elist;
	}

	@Override
	public List<EbookInfo> findAllByename(String name) {
		// TODO Auto-generated method stub
		List<EbookInfo> elist = new ArrayList<EbookInfo>();
		EbookInfo ei = null;
		conn = DBHelper.getCon();
		String sql = "select * from ebook where e_name like '%" + name + "%'";
		try {
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				ei = new EbookInfo();
				ei.setEiId(rs.getInt(1));
				ei.setEiName(rs.getString(2));
				ei.setEiFileName(rs.getString(3));
				ei.setEiTime(rs.getString(4));
				ei.setEiCount(rs.getInt(5));
				ei.setEiSize(rs.getString(6));
				elist.add(ei);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBHelper.realease(conn);
			DBHelper.closePreparedStatement(ps);
			DBHelper.closeResultSet(rs);
		}

		return elist;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		int a=0;
		Connection conn=DBHelper.getCon();
		String sql="select COUNT(*) from ebook";
		try {
			PreparedStatement ps=conn.prepareStatement(sql);
			ResultSet rs=ps.executeQuery();
			while(rs.next()){
				a=rs.getInt(1);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			DBHelper.realease(conn);
		}
		
		return a;
	}

	@Override
	public int deleteEbook(int id) {
		// TODO Auto-generated method stub
		int a = 0;
		Connection conn = DBHelper.getCon();
		String sql = "delete from ebook where e_id="+id;
		try {
			ps = conn.prepareStatement(sql);
			a = ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return a;
	}
}
