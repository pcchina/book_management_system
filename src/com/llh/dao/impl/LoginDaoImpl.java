package com.llh.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.llh.dao.LoginDao;
import com.llh.entity.AdminManagerInfo;
import com.llh.entity.SuperManagerInfo;
import com.llh.util.DBHelper;

public class LoginDaoImpl implements LoginDao {
	PreparedStatement ps = null;
	ResultSet rs = null;
	AdminManagerInfo ami = null;
	SuperManagerInfo smi = null;
	Connection conn = DBHelper.getCon();

	@Override
	public AdminManagerInfo adminLogin(String name, String pwd) {
		String sql = "select * from adminmanager where am_name='" + name + "' and am_password='" + pwd + "'";
		try {
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			if (rs.next()) {
				ami = new AdminManagerInfo();
				ami.setAmiId(rs.getInt(1));
				ami.setAmiName(rs.getString(2));
				ami.setAmiPwd(rs.getString(3));
				ami.setAmiState(rs.getInt(4));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return ami;
	}

	@Override
	public SuperManagerInfo superLogin(String name, String pwd) {
		// TODO Auto-generated method stub
		String sql = "select * from supermanager where sm_name='" + name + "' and sm_password='" + pwd + "'";
		try {
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			if (rs.next()) {
				smi = new SuperManagerInfo();
				smi.setSmiId(rs.getInt(1));
				smi.setSmiName(rs.getString(2));
				smi.setSmiPwd(rs.getString(3));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return smi;
	}

	@Override
	public List<AdminManagerInfo> findAllAdmin() {
		// TODO Auto-generated method stub
		List<AdminManagerInfo> alist = new ArrayList<AdminManagerInfo>();
		AdminManagerInfo am = null;
		String sql = "select * from adminmanager";
		try {
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				am = new AdminManagerInfo();
				am.setAmiId(rs.getInt(1));
				am.setAmiName(rs.getString(2));
				am.setAmiPwd(rs.getString(3));
				am.setAmiState(rs.getInt(4));
				alist.add(am);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBHelper.realease(conn);
			DBHelper.closePreparedStatement(ps);
			DBHelper.closeResultSet(rs);
		}
		return alist;
	}

	@Override
	public int updatePwd(int id, String pwd) {
		// TODO Auto-generated method stub
		int a = 0;
		String sql = "update adminmanager set am_password=" + pwd + " where am_id=" + id;
		try {
			ps = conn.prepareStatement(sql);
			a = ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBHelper.realease(conn);
			DBHelper.closePreparedStatement(ps);
		}
		return a;
	}

	@Override
	public int unlock(int state, int id) {
		// TODO Auto-generated method stub
		int a = 0;
		String sql = "update adminmanager set am_state=" + state + " where am_id=" + id;
		try {
			ps = conn.prepareStatement(sql);
			a = ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBHelper.realease(conn);
			DBHelper.closePreparedStatement(ps);
		}
		return a;
	}

	@Override
	public int deleteNote(int id) {
		// TODO Auto-generated method stub
		int a = 0;
		Connection conn = DBHelper.getCon();
		String sql = "delete from note where am_id=" + id;
		try {
			ps = conn.prepareStatement(sql);
			a = ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return a;
	}

	@Override
	public int delete(int id) {
		// TODO Auto-generated method stub
		int a = 0;
		Connection conn = DBHelper.getCon();
		String sql = "delete from adminmanager where am_id=" + id;
		try {
			ps = conn.prepareStatement(sql);
			a = ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return a;
	}

}
