package com.llh.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.llh.dao.BookMethodDao;
import com.llh.entity.BookInfo;
import com.llh.entity.BookTypeInfo;
import com.llh.util.DBHelper;

public class BookMethodDaoImpl implements BookMethodDao {
	Connection conn = null;
	PreparedStatement ps = null;
	ResultSet rs = null;

	@Override
	public List<BookInfo> findAllBook(int pageCode, int pageSize) {
		// TODO Auto-generated method stub
		List<BookInfo> blist = new ArrayList<BookInfo>();
		conn = DBHelper.getCon();
		int a = pageSize * (pageCode - 1) + 1;
		String sql = "select b.*,bt.bt_name from book b,booktype bt where b.b_btid=bt.bt_id and b.b_id between " + a
				+ " and " + pageSize * pageCode + "";
		try {
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				BookInfo bi = new BookInfo();
				bi.setBookId(rs.getInt(1));
				bi.setBookName(rs.getString(2));
				bi.setBookAuthor(rs.getString(3));
				bi.setBooktId(rs.getInt(4));
				bi.setBookIsbn(rs.getString(5));
				bi.setBookIntroduce(rs.getString(6));
				bi.setBookPage(rs.getInt(7));
				bi.setBookFontNumber(rs.getString(8));
				bi.setBookPublisher(rs.getString(9));
				bi.setBookIntime(rs.getString(10));
				bi.setBookNewbooks(rs.getInt(11));
				bi.setBookState(rs.getInt(12));
				bi.setBookNum(rs.getInt(13));
				bi.setBookNumAll(rs.getInt(14));
				bi.setBookTypeName(rs.getString(15));
				blist.add(bi);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBHelper.realease(conn);
			DBHelper.closePreparedStatement(ps);
			DBHelper.closeResultSet(rs);
		}
		return blist;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		int a = 0;
		Connection conn = DBHelper.getCon();
		String sql = "select COUNT(*) from book";
		try {
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				a = rs.getInt(1);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBHelper.realease(conn);
			DBHelper.closePreparedStatement(ps);
			DBHelper.closeResultSet(rs);
		}

		return a;
	}

	@Override
	public BookInfo findBookById(int id) {
		// TODO Auto-generated method stub
		BookInfo bi = null;
		conn = DBHelper.getCon();
		String sql = "select * from book where b_id=" + id;
		try {
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			if (rs.next()) {
				bi = new BookInfo();
				bi.setBookId(rs.getInt(1));
				bi.setBookName(rs.getString(2));
				bi.setBookAuthor(rs.getString(3));
				bi.setBooktId(rs.getInt(4));
				bi.setBookIsbn(rs.getString(5));
				bi.setBookIntroduce(rs.getString(6));
				bi.setBookPage(rs.getInt(7));
				bi.setBookFontNumber(rs.getString(8));
				bi.setBookPublisher(rs.getString(9));
				bi.setBookIntime(rs.getString(10));
				bi.setBookNewbooks(rs.getInt(11));
				bi.setBookState(rs.getInt(12));
				bi.setBookNum(rs.getInt(13));
				bi.setBookNumAll(rs.getInt(14));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBHelper.realease(conn);
			DBHelper.closePreparedStatement(ps);
			DBHelper.closeResultSet(rs);
		}
		return bi;
	}

	@Override
	public int updateById(BookInfo bi) {
		// TODO Auto-generated method stub
		int a = 0;
		conn = DBHelper.getCon();
		String sql = "update book set b_name='" + bi.getBookName() + "',b_author='" + bi.getBookAuthor() + "',b_isbn='"
				+ bi.getBookIsbn() + "',b_pages=" + bi.getBookPage() + ",b_fontnumber='" + bi.getBookFontNumber()
				+ "',b_publisher='" + bi.getBookPublisher() + "',b_booknumall=" + bi.getBookNumAll() + " where b_id="
				+ bi.getBookId();
		try {
			ps = conn.prepareStatement(sql);
			a = ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBHelper.realease(conn);
			DBHelper.closePreparedStatement(ps);
		}
		return a;
	}

	@Override
	public int update(BookInfo bin, int bookNum) {
		// TODO Auto-generated method stub
		int a = 0;
		conn = DBHelper.getCon();
		String sql = "update book set b_booknum=" + bookNum + " where b_id=" + bin.getBookId();
		try {
			ps = conn.prepareStatement(sql);
			a = ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBHelper.realease(conn);
			DBHelper.closePreparedStatement(ps);
		}
		return a;
	}

	@Override
	public int deleteById(int id) {
		// TODO Auto-generated method stub
		int a = 0;
		conn = DBHelper.getCon();
		String sql = "delete from book where b_id=" + id;
		try {
			ps = conn.prepareStatement(sql);
			a = ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBHelper.realease(conn);
			DBHelper.closePreparedStatement(ps);
		}
		return a;
	}

	@Override
	public List<BookInfo> findAllByName(String name) {
		// TODO Auto-generated method stub
		List<BookInfo> blist = new ArrayList<BookInfo>();
		BookInfo bi = null;
		conn = DBHelper.getCon();
		String sql = "select b.*,bt.bt_name from book b,booktype bt where b.b_btid=bt.bt_id and b.b_name like '%" + name
				+ "%'";
		try {
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				bi = new BookInfo();
				bi.setBookId(rs.getInt(1));
				bi.setBookName(rs.getString(2));
				bi.setBookAuthor(rs.getString(3));
				bi.setBooktId(rs.getInt(4));
				bi.setBookIsbn(rs.getString(5));
				bi.setBookIntroduce(rs.getString(6));
				bi.setBookPage(rs.getInt(7));
				bi.setBookFontNumber(rs.getString(8));
				bi.setBookPublisher(rs.getString(9));
				bi.setBookIntime(rs.getString(10));
				bi.setBookNewbooks(rs.getInt(11));
				bi.setBookState(rs.getInt(12));
				bi.setBookNum(rs.getInt(13));
				bi.setBookNumAll(rs.getInt(14));
				bi.setBookTypeName(rs.getString(15));
				blist.add(bi);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBHelper.realease(conn);
			DBHelper.closePreparedStatement(ps);
			DBHelper.closeResultSet(rs);
		}

		return blist;
	}

	@Override
	public List<BookTypeInfo> findBookType() {
		// TODO Auto-generated method stub
		List<BookTypeInfo> btlist = new ArrayList<BookTypeInfo>();
		BookTypeInfo bti = null;
		conn = DBHelper.getCon();
		String sql = "select * from booktype";
		try {
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				bti = new BookTypeInfo();
				bti.setBtiId(rs.getInt(1));
				bti.setBtiName(rs.getString(2));
				btlist.add(bti);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBHelper.realease(conn);
			DBHelper.closePreparedStatement(ps);
			DBHelper.closeResultSet(rs);
		}
		return btlist;
	}

	@Override
	public int addBook(BookInfo bif) {
		// TODO Auto-generated method stub
		int a = 0;
		conn = DBHelper.getCon();
		String sql = "insert into book(b_name,b_author,b_btid,b_isbn,b_introduce,b_pages,b_fontnumber,b_publisher,b_intime,b_booknum,b_booknumall) values('"
				+ bif.getBookName() + "','" + bif.getBookAuthor() + "'," + bif.getBooktId() + ",'" + bif.getBookIsbn()
				+ "','" + bif.getBookIntroduce() + "'," + bif.getBookPage() + ",'" + bif.getBookFontNumber() + "','"
				+ bif.getBookPublisher() + "',now(),"+bif.getBookNumAll()+"," + bif.getBookNumAll() + ")";
		try {
			ps = conn.prepareStatement(sql);
			a = ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBHelper.realease(conn);
			DBHelper.closePreparedStatement(ps);
		}
		return a;
	}

	@Override
	public int updateState(int state, int id) {
		// TODO Auto-generated method stub
		int a = 0;
		conn = DBHelper.getCon();
		String sql = "update book set b_bookstate=" + state + " where b_id=" + id;
		try {
			ps = conn.prepareStatement(sql);
			a = ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBHelper.realease(conn);
			DBHelper.closePreparedStatement(ps);
		}
		return a;
	}

	@Override
	public List<BookInfo> findAllComm(int pageCode, int pageSize) {
		// TODO Auto-generated method stub
		List<BookInfo> blist = new ArrayList<BookInfo>();
		int a = pageSize * (pageCode - 1);
		BookInfo bi = null;
		conn = DBHelper.getCon();
		String sql = "select b.*,bt.bt_name from book b,booktype bt where b.b_bookstate=1 and b.b_btid=bt.bt_id and b.b_id limit " + a
				+ "," + pageSize + "";
		try {
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				bi = new BookInfo();
				bi.setBookId(rs.getInt(1));
				bi.setBookName(rs.getString(2));
				bi.setBookAuthor(rs.getString(3));
				bi.setBooktId(rs.getInt(4));
				bi.setBookIsbn(rs.getString(5));
				bi.setBookIntroduce(rs.getString(6));
				bi.setBookPage(rs.getInt(7));
				bi.setBookFontNumber(rs.getString(8));
				bi.setBookPublisher(rs.getString(9));
				bi.setBookIntime(rs.getString(10));
				bi.setBookNewbooks(rs.getInt(11));
				bi.setBookState(rs.getInt(12));
				bi.setBookNum(rs.getInt(13));
				bi.setBookNumAll(rs.getInt(14));
				bi.setBookTypeName(rs.getString(15));
				blist.add(bi);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBHelper.realease(conn);
			DBHelper.closePreparedStatement(ps);
			DBHelper.closeResultSet(rs);
		}

		return blist;
	}

	@Override
	public int getCount1() {
		// TODO Auto-generated method stub
		int a = 0;
		Connection conn = DBHelper.getCon();
		String sql = "select COUNT(*) from book where b_bookstate=1";
		try {
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				a = rs.getInt(1);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBHelper.realease(conn);
			DBHelper.closePreparedStatement(ps);
			DBHelper.closeResultSet(rs);
		}

		return a;
	}

	@Override
	public List<BookInfo> findAllBorrow(int pageCode, int pageSize) {
		// TODO Auto-generated method stub
		List<BookInfo> blist = new ArrayList<BookInfo>();
		conn = DBHelper.getCon();
		int a = pageSize * (pageCode - 1);
		String sql = "select b.*,bt.bt_name from book b inner join booktype bt on b.b_btid=bt.bt_id order by(b.b_booknumall-b.b_booknum)desc limit "+a+","+pageSize+"";
		try {
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				BookInfo bi = new BookInfo();
				bi.setBookId(rs.getInt(1));
				bi.setBookName(rs.getString(2));
				bi.setBookAuthor(rs.getString(3));
				bi.setBooktId(rs.getInt(4));
				bi.setBookIsbn(rs.getString(5));
				bi.setBookIntroduce(rs.getString(6));
				bi.setBookPage(rs.getInt(7));
				bi.setBookFontNumber(rs.getString(8));
				bi.setBookPublisher(rs.getString(9));
				bi.setBookIntime(rs.getString(10));
				bi.setBookNewbooks(rs.getInt(11));
				bi.setBookState(rs.getInt(12));
				bi.setBookNum(rs.getInt(13));
				bi.setBookNumAll(rs.getInt(14));
				bi.setBookTypeName(rs.getString(15));
				blist.add(bi);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBHelper.realease(conn);
			DBHelper.closePreparedStatement(ps);
			DBHelper.closeResultSet(rs);
		}
		return blist;
	}

	@Override
	public int getCount2() {
		// TODO Auto-generated method stub
		int a = 0;
		Connection conn = DBHelper.getCon();
		String sql = "select COUNT(*) from book";
		try {
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				a = rs.getInt(1);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBHelper.realease(conn);
			DBHelper.closePreparedStatement(ps);
			DBHelper.closeResultSet(rs);
		}

		return a;
	}

	@Override
	public int dropBorrow(int id,int count) {
		// TODO Auto-generated method stub
		int a = 0;
		conn = DBHelper.getCon();
		String sql = "update book set b_booknum="+count+" where b_id=" + id;
		try {
			ps = conn.prepareStatement(sql);
			a = ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBHelper.realease(conn);
			DBHelper.closePreparedStatement(ps);
		}
		return a;
	}

}
