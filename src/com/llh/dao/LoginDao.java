package com.llh.dao;

import java.util.List;

import com.llh.entity.AdminManagerInfo;
import com.llh.entity.SuperManagerInfo;

public interface LoginDao {
	// 普通管理员登录
	public AdminManagerInfo adminLogin(String name, String pwd);

	// 超级管理员登录
	public SuperManagerInfo superLogin(String name, String pwd);

	// 超管查询所有管理员
	public List<AdminManagerInfo> findAllAdmin();

	// 超管修改管理员密码
	public int updatePwd(int id, String pwd);

	// 超管解除管理员的锁定
	public int unlock(int state, int id);

	// 删除账号前 清空此账号下的所有留言
	public int deleteNote(int id);

	// 删除账号
	public int delete(int id);
}
