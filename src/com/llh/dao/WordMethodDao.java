package com.llh.dao;

import java.util.List;

import com.llh.entity.NoteInfo;

public interface WordMethodDao {

	// 普通管理员添加留言
	public int addWord(NoteInfo ni);

	// 查询自己的留言信息
	public List<NoteInfo> findSelfWord(int id, int pageCode, int pageSize);

	// 通过id获取总条数
	public int getCount(int id);
	
	//通过id查询留言的详细信息
	public NoteInfo findNoteByid(int id);
	
	//获得总条数
	public int getCount1();
	
	//获得所有留言
	public List<NoteInfo> findAllNote(int pageCode, int pageSize);
	
	//通过姓名查询留言
	public List<NoteInfo> findNoteByname(String name);
	
	//审核留言通过
	public int checkNote(int id);
	
	//删除留言
	public int deleteNote(int id);
	
}
