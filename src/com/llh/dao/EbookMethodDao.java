package com.llh.dao;

import java.util.List;

import com.llh.entity.EbookInfo;

public interface EbookMethodDao {
	
	//上传
	public int upload(String ename,String efileName,String path);
	
	//查询所有电子书
	public List<EbookInfo> findAll(int pageCode,int pageSize);
	
	//通过书名查询电子书
	public List<EbookInfo> findAllByename(String name);

	//通过id删除某本电子书
	public int deleteEbook(int id);
	
	//获取总条数
	public int getCount();
	

	

}
