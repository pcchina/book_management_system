package com.llh.dao;

import java.util.List;

import com.llh.entity.BookInfo;
import com.llh.entity.BookTypeInfo;

public interface BookMethodDao {

	// 查询所有图书
	public List<BookInfo> findAllBook(int pageCode, int pageSize);

	// 获取总条数
	public int getCount();

	// 通过id查图书
	public BookInfo findBookById(int id);

	// 通过id修改数据
	public int updateById(BookInfo bi);

	// 修改总的书本数量之后，修改剩余书本数量
	public int update(BookInfo bin, int bookNum);
	
	//删除图书
	public int deleteById(int id);
	
	//通过name模糊查询
	public List<BookInfo> findAllByName(String name);
	
	//查询出书的类型
	public List<BookTypeInfo> findBookType();
	
	//添加图书
	public int addBook(BookInfo bif);
	
	//根据id修改图书的状态
	public int updateState(int state,int id);
	
	//查询所有推荐书
	public List<BookInfo> findAllComm(int pageCode,int pageSize);
	
	// 获取总条数
	public int getCount1();
	
	//查询所有介借阅书
	public List<BookInfo> findAllBorrow(int pageCode,int pageSize);
	
	// 获取总条数
	public int getCount2();
	
	//清空借出书的数量
	public int dropBorrow(int id,int count);
		

}
