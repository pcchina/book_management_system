# 毕设-图书管理系统

#### 介绍
大学学的那一点点东西，当时的毕业设计。

#### 软件架构
大学毕业前的第一桶金。纪念一下


#### 使用说明
1、项目文件名叫“BookManager”

2、建议使用eclipse-jee-neon-3-win32-x86_64(霓虹灯3)版本部署(部署记得更改编码格式，默认为GBK,应该改为utf-8)

3、jdk-8u131-windows-i586_8.0.1310.11(jdk版本必须为1.8以上)

4、apache-tomcat-9.0.0.M10-windows-x64服务器(tomcat 7.0,tomcat 8.0也可以)，

5、mysql-installer-community-5.7.17.0数据库，

6、数据库的可视化工具建议使用MySQL-Front，

7、数据库连接用户名为：root;
密码为：123456；

8、如果不一致，请打开项目，在com.llh.util包中找到DBHelper类，更改其中的用户名或者密码
部署好项目后建议使用Google浏览器直接键入地址
http://localhost:8080/BookManager


超级管理员
用户名：llh
密码：111111

普通管理员
（未锁定）
用户名：admin
密码：123456
（已锁定）
用户名：admin3
密码：123456

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)