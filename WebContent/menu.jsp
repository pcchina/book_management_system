<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>图书管理系统</title>
<link href="css/main.css" rel="stylesheet" type="text/css">
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery-2.1.1.js"></script>
</head>
<script type="text/javascript">
$(function() {
	$("[class=word]").click(function () {
		$("[id=word]").html("");
	});
});
</script>
<script type="text/javascript">
	function time() {
		var div_time = document.getElementById('showtime');
		var now = new Date();
		div_time.innerHTML = "Time:" + now.toLocaleString()
		setTimeout(time, 1000);
	}
</script>
<script type="text/javascript" src="js/left.js">

</script>
<body onload="time()">
	<div id="all">
		<div id="top">
			<div id="top1">
				<div id="showtime"
					style="text-align: right; color: white; font-size: 20px; padding-top: 15px;">
				</div>
			</div>
			<div id="top2">
				<div
					style="font-size: 40px; font-family: '楷体'; margin-left: 50px; padding-top: 15px;">图书管理系统</div>
				<div style="float: right;">
					欢迎您：${am.amiName }${sm.smiName }&nbsp;|&nbsp;登陆时间：${d.getYear()-100}年/${d.getMonth()+1}月/${d.getDate() }日&nbsp;|&nbsp;
					<c:if test="${!empty am.amiName }"> <a
						href="updateadmin.jsp?id=${am.amiId }&&pwd=${am.amiPwd }" style="color: white;" target="main">修改密码</a>&nbsp;|&nbsp;</c:if><a
						href="javascript:if(confirm('确认要退出？')) location.href='exit.action'" style="color: white;">安全退出</a>
				</div>
			</div>
		</div>
		<div id="content">
			<div id="left">
		<c:if test="${!empty am.amiName }">
				<c:if test="${am.amiState != 1 }">
				<ul class="nav nav-pills nav-stacked">
					<li><a href="main.jsp" target="main">首页</a></li>
				</ul>
				<ul class="b1 nav nav-pills nav-stacked">
					<li><a href="#" name="more_btn" onclick="ai('b2')">图书管理</a></li>
					<ul class="nav nav-pills nav-stacked" id="b2" style="display: none;margin-left: 20px;font-size: 18px;">
						<li><a href="findBookType.action" target="main">添加图书</a></li>
						<li><a href="findAllBook.action" target="main">查看图书</a></li>
						<li><a href="commendBook.action" target="main">推荐书管理</a></li>
						<li><a href="borrowBook.action" target="main">借书管理</a></li>
					</ul>
				</ul>
				<ul class="nav nav-pills nav-stacked">
					<li><a href="#" onclick="ai('b3')">电子书管理</a></li>
					<ul class="nav nav-pills nav-stacked" id="b3" style="display: none;margin-left: 20px;font-size: 18px;">
						<li><a href="upload.jsp" target="main">上传电子书</a></li>
						<li><a href="findAll.action" target="main">查看电子书</a></li>
					</ul>
				</ul>
				<ul class="nav nav-pills nav-stacked">
					<li><a href="#"  onclick="ai('b6')" class="word">留言</a></li>
					<ul class="nav nav-pills nav-stacked" id="b6" style="display: none;margin-left: 20px;font-size: 18px;">
						<li><a href="word.jsp?id=${am.amiId }&&name=${am.amiName }" target="main">添加留言</a></li>
						<li><a href="findmyselfword.action?id=${am.amiId }" target="main"><span id="word" class="badge pull-right">4</span>查看个人留言</a></li>
					</ul>
				</ul>
				</c:if>
				<c:if test="${am.amiState eq 1 }">
					<ul class="nav nav-pills nav-stacked">
						<li><h3>您的账号已锁定，请申请解锁！</h3></li>
						<li><a href="word.jsp?id=${am.amiId }&&name=${am.amiName }" target="main">申请解锁</a></li>
					</ul>
				</ul>
				</c:if>
			</c:if>
			<c:if test="${!empty sm.smiName }">
			<ul class="nav nav-pills nav-stacked">
					<li><a href="main.jsp" target="main">首页</a></li>
				</ul>
				<ul class="nav nav-pills nav-stacked">
					<li><a href="#"  onclick="ai('b7')" class="word">系统管理</a></li>
					<ul class="nav nav-pills nav-stacked" id="b7" style="display: none;margin-left: 20px;font-size: 18px;">
						<li><a href="manager.action" target="main">管理管理员</a></li>
						<li><a href="findAllNote.action" target="main"><span id="word" class="badge pull-right">4</span>查看留言</a></li>
					</ul>
				</ul>
			</c:if>
			</div>
			<div id="right">
				<iframe name="main" frameborder="1" src="main.jsp" scrolling="1" width="100%"
					height="100%"></iframe>
			</div>
		</div>
		<div id="bottom">
		<div style="margin-left: 50%;margin-top: -180px;">
			<p style="padding-top: 30px;"><b>图书管理系统&nbsp;&nbsp;||&copy;&nbsp;&nbsp;2017||&nbsp;&nbsp;京ICP证030156号</b></p>
			<p></p>
		</div>
		</div>
	</div>
</body>

</html>