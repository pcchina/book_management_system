<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>留言功能</title>
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/bootstrap.js"></script>
<script type="text/javascript" src="js/jquery-2.1.1.js"></script>
<script type="text/javascript">
	$(function () {
		$("[id=add]").click(function () {
			var a = document.getElementById("title1").value;
			var b = document.getElementById("content").value;
			if(a.length==0){
				$("[id=help]").html("标题不能为空");
				return false;
			}else if(a.length>=10){
				$("[id=help]").html("标题必须十个字之内");
				return false;
			}else if(b.length==0){
				$("[id=help1]").html("内容不能为空");
				return false;
			}else if(b.length>=50){
				$("[id=help1]").html("内容必须五十个字之内");
				return false;
			}
		});
	});
</script>
</head>
<body>
	<div class="jumbotron">
		<div style="margin-left: 100px;">
			<h1>欢迎留言！</h1>
		</div>
	</div>
	<form action="addword.action" class="form-horizontal" role="form" method="post">
		<div style="width: 800px; margin-left: 60px;">
			<div class="form-group">
				<label for="inputPassword" class="col-sm-2 control-label">姓名：</label>
				<div class="col-sm-10">
				<input type="hidden" name="ni.adimnId" value="${param.id }" />
					<input type="text" class="form-control" name="ni.noteAuthor"
					 readonly="readonly" value="${param.name }">
				</div>
			</div>
			<div class="form-group">
				<label for="inputPassword" class="col-sm-2 control-label">标题：</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="title1" name="ni.noteTitle"
						placeholder="不超过10个字">
						<span class="help-block" id="help" style="color: red;"></span>
				</div>
			</div>
			<div class="form-group">
				<label for="inputPassword" class="col-sm-2 control-label">内容：</label>
				<div class="col-sm-10">
					<textarea class="form-control" id="content" name="ni.noteContent" rows="5px;"></textarea>
					<span class="help-block" id="help1" style="color: red;"></span>
				</div>
			</div>
			<div style="margin-left: 50%">
				<button type="submit" class="btn btn-primary" id="add">提交</button>
				<button type="reset" class="btn btn-default">重填</button>
			</div>
		</div>
	</form>
</body>
</html>