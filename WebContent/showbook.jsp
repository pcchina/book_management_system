<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" />
</head>
<script type="text/javascript" src="js/jquery-1.8.3.js"></script>
<script type="text/javascript" type="text/javascript">
	$(function() {
		$("#page").blur(function() {
			location.href = "findAllBook.action?pageCode=" + $("#page").val();
		});
	});
	$(function() {
		$("[id=find]").click(function() {
			var a = document.getElementById("name").value;
			if (a.length == 0) {
				alert("请输入关键字");
				return false;
			}
		});
	});
</script>
<body style="font-size: 15px; font-family: '楷体';">
	<form action="findAllByname" method="post">
		<table class="table table-bordered">
			<tr>
				<td><input type="text" id="name" name="ename"
					placeholder="输入部分或全部书名" /> <input type="submit"
					class="btn btn-warning" id="find" value="查询" /></td>
			</tr>
		</table>
	</form>
	<table class="table table-bordered">
		<thead>
			<tr style="text-align: center;" class="success">
				<td>编号</td>
				<td>书名</td>
				<td>作者</td>
				<td>类型</td>
				<td>ISBN号</td>
				<td>简介</td>
				<td>页数</td>
				<td>字数</td>
				<td>出版社</td>
				<td>入库时间</td>
				<td>图书总数</td>
				<td>剩余数量</td>
				<td>操作</td>
			</tr>
		</thead>
		<tbody>
			<c:if test="${!empty bi }">
				<tr>
					<td colspan="13">查询到的图书</td>
				</tr>
				<c:forEach items="${bi }" var="bi">

					<tr style="text-align: center;">
						<td>${bi.bookId }</td>
						<td>${bi.bookName }</td>
						<td>${bi.bookAuthor }</td>
						<td>${bi.bookTypeName }</td>
						<td>${bi.bookIsbn }</td>
						<td>${bi.bookIntroduce}</td>
						<td>${bi.bookPage }</td>
						<td>${bi.bookFontNumber }</td>
						<td>${bi.bookPublisher }</td>
						<td>${bi.bookIntime }</td>
						<td>${bi.bookNumAll }</td>
						<td>${bi.bookNum }</td>
						<td style="text-align: center;"><a
							href="updatebook.action?id=${bi.bookId }"><button
									class="btn btn-primary">修改图书</button></a>&nbsp;&nbsp;<a
							href="javascript:if (confirm('确定要删除吗？')) location.href='deletebook.action?id=${bi.bookId }'"><button
									class="btn btn-danger">删除图书</button></a></td>
					</tr>
				</c:forEach>
			</c:if>
			<tr>
				<td colspan="13">全部图书</td>
			</tr>
			<c:forEach items="${blist }" var="b">

				<tr style="text-align: center;">
					<td>${b.bookId }</td>
					<td>${b.bookName }</td>
					<td>${b.bookAuthor }</td>
					<td>${b.bookTypeName }</td>
					<td>${b.bookIsbn }</td>
					<td>${b.bookIntroduce }</td>
					<td>${b.bookPage }</td>
					<td>${b.bookFontNumber }</td>
					<td>${b.bookPublisher }</td>
					<td>${b.bookIntime }</td>
					<td>${b.bookNumAll }</td>
					<td>${b.bookNum }</td>
					<td style="text-align: center;"><a
						href="updatebook.action?id=${b.bookId }"><button
								class="btn btn-primary">修改图书</button></a>&nbsp;&nbsp;<a
						href="javascript:if (confirm('确定要删除吗？')) location.href='deletebook.action?id=${b.bookId }'"><button
								class="btn btn-danger">删除图书</button></a></td>
				</tr>
			</c:forEach>
			<tr>
				<td colspan="13" style="text-align: center;"><a
					href="findAllBook.action?pageCode=1">首页</a>第${pageCode}页 <a
					href="findAllBook.action?pageCode=${pageCode-1 }">上一页</a> <a
					href="findAllBook.action?pageCode=${pageCode+1 }">下一页</a> <a
					href="findAllBook.action?pageCode=${totalCode}">尾页</a> 到第<input
					type="text" name="page" id="page" size="2" />页 共${totalCode}页</td>
			</tr>
		</tbody>
	</table>
</body>
</html>