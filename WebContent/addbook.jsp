<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="js/addbook.js"></script>
</head>
<body>
	<form action="addbook.action" class="form-horizontal" method="post">
		<div class="form-group" style="width: 500px; margin: 100px auto;">
			<label class="col-sm-2 control-label">书名：</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" id="name"
					name="bif.bookName" placeholder="请输入书名"><span></span>
			</div>
			<label class="col-sm-2 control-label" style="padding-top: 20px;">类型：</label>
			<div class="col-sm-10" style="padding-top: 15px;">
				<select class="form-control" name="bif.booktId" id="btype">
					<option value="0">请选择类型</option>
					<c:forEach items="${bti }" var="b">
						<option value="${b.btiId }">${b.btiName }</option>
					</c:forEach>
				</select>
			</div>
			<label class="col-sm-2 control-label" style="padding-top: 20px;">作者：</label>
			<div class="col-sm-10" style="padding-top: 15px;">
				<input type="text" class="form-control" name="bif.bookAuthor"
					id="author" placeholder="请输入作者">
			</div>
			<label class="col-sm-2 control-label" style="padding-top: 20px;">ISBN：</label>
			<div class="col-sm-10" style="padding-top: 15px;">
				<input type="text" class="form-control" name="bif.bookIsbn"
					id="isbn" placeholder="请输入ISBN">
			</div>
			<label class="col-sm-2 control-label" style="padding-top: 20px;">简介：</label>
			<div class="col-sm-10" style="padding-top: 15px;">
				<textarea class="form-control" name="bif.bookIntroduce" rows="3"
					id="introduce" placeholder="请输入书本简介(不少于10个不大于25个字符)"></textarea>
			</div>
			<label class="col-sm-2 control-label" style="padding-top: 20px;">页数：</label>
			<div class="col-sm-10" style="padding-top: 15px;">
				<input type="text" class="form-control" name="bif.bookPage"
					id="pagecount" placeholder="请输入页数">
			</div>
			<label class="col-sm-2 control-label" style="padding-top: 20px;">字数：</label>
			<div class="col-sm-10" style="padding-top: 15px;">
				<input type="text" class="form-control" name="bif.bookFontNumber"
					id="fontcount" placeholder="请输入字数">
			</div>
			<label class="col-sm-2 control-label" style="padding-top: 20px;">出版：</label>
			<div class="col-sm-10" style="padding-top: 15px;">
				<input type="text" class="form-control" name="bif.bookPublisher"
					id="publisher" placeholder="请输入出版社">
			</div>
			<label class="col-sm-2 control-label" style="padding-top: 20px;">数量：</label>
			<div class="col-sm-10" style="padding-top: 15px;">
				<input type="text" class="form-control" name="bif.bookNumAll"
					id="count" placeholder="请输入数量">
			</div>
		</div>
		<div class="form-group" style="width: 200px; margin: -60px auto;">
			<div class="col-sm-offset-2 col-sm-10">
				<button type="submit" class="btn btn-primary" id="add">添加</button>
				<button type="reset" class="btn btn-danger">重填</button>
			</div>
		</div>
	</form>
</body>
</html>