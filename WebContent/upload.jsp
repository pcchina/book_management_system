<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap.theme.css" rel="stylesheet" type="text/css" />
</head>
<script src="js/jquery-1.8.3.js"></script>
<script type="text/javascript">
	$(function() {
		$("[class=upload]").click(function() {
			var checkinput = document.getElementById("doc").value;
			if (checkinput.length == 0) {
				alert("请选择文件上传！");
				return false;
			}
		});
	});
	function addfile() {
		var i = document.createElement("input");
		i.setAttribute("type", "file");
		i.setAttribute("name", "doc");
		var d = document.createElement("div");

		document.getElementById("up").appendChild(i);
		document.getElementById("up").appendChild(d);
	}
</script>
<body>
	<form action="upload" method="post" enctype="multipart/form-data">
		<div>
			<h2>仅支持.txt,.docx，大小为20M以下的文件上传！(.txt文件只能在线预览，.docx文件可以下载)</h2>
		</div>
		<div id="up" class="form-group">
			<label for="inputfile">文件输入</label>
			<input type="file" name="doc" id="doc" /><br />
		</div>
		<a href="javascript:addfile()">继续上传</a> <input class="upload"
			type="submit" value="上传" />
	</form>
</body>
</html>