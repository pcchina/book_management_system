<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" />
</head>
<script type="text/javascript" src="js/jquery-1.8.3.js"></script>
<script type="text/javascript" type="text/javascript">
	$(function() {
		$("#page").blur(function() {
			location.href = "showcomm.action?pageCode=" + $("#page").val();
		});
	});
</script>
<body style="font-size: 15px; font-family: '楷体';">
	<table class="table table-bordered">
		<thead>
			<tr class="success">
				<td colspan="12">
					<form action="showcomm.action" method="post">
						查看推荐书:<input type="submit" class="btn btn-warning" value="查询" />
					</form>
				</td>
			</tr>
			<tr style="text-align: center;">
				<td>编号</td>
				<td>书名</td>
				<td>作者</td>
				<td>类型</td>
				<td>isbn</td>
				<td>出版社</td>
				<td>入库时间</td>
				<td>图书总数</td>
				<td>已借出数</td>
				<td>剩余数量</td>
				<td>推荐书</td>
				<td>操作</td>
			</tr>
		</thead>
		<tbody>
		<!--<s:iterator value="#session.blist" var="b">
			<tr>
				<td><s:property value="#b.bookId"/></td>
			</tr>
		</s:iterator>-->
		<c:forEach items="${blist }" var="b">
			<tr style="text-align: center;">
			<td>${b.bookId }</td>
				<td>${b.bookName }</td>
				<td>${b.bookAuthor }</td>
				<td>${b.bookTypeName }</td>
				<td>${b.bookIsbn }</td>
				<td>${b.bookPublisher }</td>
				<td>${b.bookIntime }</td>
				<td>${b.bookNumAll }</td>
				<td>${b.bookNumAll-b.bookNum }</td>
				<td>${b.bookNum }</td>
				<td><c:if test="${b.bookState eq 1 }">是</c:if>
				<c:if test="${b.bookState eq 0 }">否</c:if></td>
				<td>
				<a href="javascript:if (confirm('确定不再推荐？')) location.href='updatestate1.action?comId=${b.bookId }'"><input type="button" class="btn btn-danger" value="不推荐" /></a>
				</td>
			</tr>
		</c:forEach>
			<tr>
				<td colspan="13" style="text-align: center;"><a
					href="showcomm.action?pageCode=1">首页</a>第${pageCode}页 <a
					href="showcomm.action?pageCode=${pageCode-1 }">上一页</a> <a
					href="showcomm.action?pageCode=${pageCode+1 }">下一页</a> <a
					href="showcomm.action?pageCode=${totalCode}">尾页</a> 到第<input
					type="text" name="page" id="page" size="2" />页 共${totalCode}页</td>
			</tr>
		</tbody>
	</table>
</body>
</html>