<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" href="css/reset.css">
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" type="text/css" href="css/bootstrap-theme.css" />
<link rel="stylesheet" type="text/css" href="css/form1.css" />
<script src="js/modernizr.js"></script>
<script src="js/check.js"></script>
<script src="js/jquery-1.8.3.js"></script>
<script type="text/javascript">
	$(function() {
		$("[class=login]").click(function() {
			var checkinput = document.getElementById("uname1").value;
			var checkpwd = document.getElementById("upwd1").value;
			if (checkinput.length == 0) {
				alert("用户名不能为空！");
				return false;
			} else if (checkpwd.length == 0) {
				alert("密码不能为空！");
				return false;
			}
		});
	});
</script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
</head>
<body onload="createCode(),checkSuper()">
	<div class="projects-container">
		<ul>
			<li>
				<div class="cd-title">
					<h2>普通管理员</h2>
				</div>
				<div class="cd-project-info">
					<div class="form1" id="form1">
						<form action="AdminLogin.action" method="post">
							<table align="center">
								<tr>
									<td><span style="color: burlywood;">用户名</span></td>
									<td><input id="uname" type="text" name="ami.amiName"
										size="40px"
										style="line-height: 30px; border: solid 3px #985F0D; border-top: hidden; border-left: hidden; border-right: hidden; background-color: #95704B; opacity: 0.6;"
										placeholder="输入用户名" /></td>
								</tr>
								<tr>
									<td><span style="color: burlywood;">密码</span></td>
									<td><input id="upwd" type="password" name="ami.amiPwd"
										size="40px"
										style="line-height: 30px; border: solid 3px #985F0D; border-top: hidden; border-left: hidden; border-right: hidden; background-color: #95704B; opacity: 0.6;"
										placeholder="输入密码" /></td>
								</tr>
								<tr>
									<td><span style="color: burlywood;"> 验证码 </span></td>
									<td><input type="text" id="inputCode" placeholder="验证码"
										size="40px"
										style="line-height: 30px; border: solid 3px #985F0D; border-top: hidden; border-left: hidden; border-right: hidden; background-color: #95704B; opacity: 0.6;" />
									</td>
								</tr>
								<tr>
									<td>
										<div class="code" id="checkCode" onclick="createCode()"
											required="required" style="float: left; line-height: 4px;"></div>
									</td>
									<td><a href="#" onclick="createCode()"
										style="float: right; margin-left: 150px;">看不清换一张</a></td>
									</td>
								</tr>
								<tr>
									<td></td>
									<td><input type="submit" onclick="return validateCode()"
										value="登录"
										style="width: 300px; height: 45px; background-color: #4D2E12; opacity: 0.6;" /></td>
								</tr>

							</table>
						</form>
					</div>

				</div>
			</li>
			<li>
				<div class="cd-title">
					<h2>超级管理员</h2>
					<p></p>
				</div>

				<div class="cd-project-info">
					<div class="form1" id="form1">
						<form action="SuperLogin.action" method="post">
							<table align="center">
								<tr>
									<td><span style="color: burlywood;">用户名</span></td>
									<td><input id="uname1" type="text" name="smi.smiName"
										size="40px"
										style="line-height: 30px; border: solid 3px #985F0D; border-top: hidden; border-left: hidden; border-right: hidden; background-color: #95704B; opacity: 0.6;"
										placeholder="输入用户名" /></td>
								</tr>
								<tr>
									<td><span style="color: burlywood;">密码</span></td>
									<td><input id="upwd1" type="password" name="smi.smiPwd"
										size="40px"
										style="line-height: 30px; border: solid 3px #985F0D; border-top: hidden; border-left: hidden; border-right: hidden; background-color: #95704B; opacity: 0.6;"
										placeholder="输入密码" /></td>
								</tr>
								<tr>
									<td></td>
									<td><input class="login" type="submit" value="登录"
										style="width: 300px; height: 45px; background-color: #4D2E12; opacity: 0.6;" /></td>
								</tr>
							</table>
						</form>
					</div>
				</div>
			</li>
		</ul>
		<a href="#0" class="cd-close">Close</a> <a href="#0" class="cd-scroll">Scroll</a>
	</div>
	<script src="js/jquery-2.1.1.js"></script>
	<script src="js/main.js"></script>
</body>
</html>