<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" />
</head>
<script type="text/javascript" src="js/jquery-1.8.3.js"></script>
<script type="text/javascript" type="text/javascript">
	$(function() {
		$("#page").blur(
				function() {
					location.href = "findmyselfword.action?pageCode="
							+ $("#page").val();
				});
	});
</script>
<body style="font-size: 15px; font-family: '楷体';">
	<table class="table table-bordered">
		<tr class="success" style="text-align: center;">
			<td>留言人</td>
			<td>标题</td>
			<td>留言内容</td>
			<td>留言时间</td>
			<td>状态</td>
			<td>操作</td>
		</tr>
		<c:forEach items="${nlist }" var="n">
			<tr>
				<td>${n.noteAuthor }</td>
				<td>${n.noteTitle }</td>
				<td>${fn:substring(n.noteContent,0,5) }···</td>
				<td>${n.noteTime }</td>
				<td><c:if test="${n.noteState eq 0 }">未审核</c:if> <c:if
						test="${n.noteState eq 1 }">已审核</c:if></td>
				<td><a class="btn btn-primary" href="findNoteByid.action?noteId=${n.noteId }">详情</a></td>
			</tr>
		</c:forEach>
		<tr>
			<td colspan="13" style="text-align: center;"><a
				href="findmyselfword.action?pageCode=1">首页</a>第${pageCode}页 <a
				href="findmyselfword.action?pageCode=${pageCode-1 }">上一页</a> <a
				href="findmyselfword.action?pageCode=${pageCode+1 }">下一页</a> <a
				href="findmyselfword.action?pageCode=${totalCode}">尾页</a> 到第<input
				type="text" name="page" id="page" size="2" />页 共${totalCode}页</td>
		</tr>
	</table>
</body>
</html>