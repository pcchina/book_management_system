<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" />
</head>
<script type="text/javascript" src="js/jquery-1.8.3.js"></script>
<script type="text/javascript" type="text/javascript">
	$(function() {
		$("#page").blur(function() {
			location.href = "findAll.action?pageCode=" + $("#page").val();
		});
	});
	$(function() {
		$("[id=find]").click(function() {
			var a = document.getElementById("name").value;
			if (a.length == 0) {
				alert("请输入关键字");
				return false;
			}
		});
	});
</script>
<body style="font-size: 15px; font-family: '楷体';">
	<form action="findAllByename" method="post">
		<table>
			<tr>
				<td><input type="text" id="name" name="ename"
					placeholder="输入书名" /> <input type="submit" id="find"
					class="btn btn-warning" value="查询" /></td>
			</tr>
		</table>
	</form>
	<table class="table table-bordered">
		<thead>
			<tr style="text-align: center;" class="success">
				<td>编号</td>
				<td>电子书名</td>
				<td>文件名</td>
				<td>存入时间</td>
				<td>存入路径</td>
				<td>下载次数</td>
				<td colspan="4">操作</td>
			</tr>
		</thead>
		<tbody>
			<c:if test="${!empty ei }">
				<tr>
					<td colspan="10">查询到的电子书</td>
				</tr>
				<c:forEach items="${ei }" var="ei">
					<tr>
						<td>${ei.eiId }</td>
						<td>${ei.eiName }</td>
						<td>${ei.eiFileName }</td>
						<td>${ei.eiTime }</td>
						<td>${ei.eiSize }</td>
						<td>${ei.eiCount }</td>
						<td colspan="4" style="text-align: center;"><a
							href="file/${ei.eiName }"><button class="btn btn-primary">下载</button></a>&nbsp;&nbsp;<a
							href="javascript:if (confirm('确定要删除吗？')) location.href='deleteebook.action?id=${ei.eiId }'"><button
									class="btn btn-danger">删除</button></a></td>
					</tr>
				</c:forEach>
			</c:if>
			<tr>
				<td colspan="10">全部电子书</td>
			</tr>
			<c:forEach items="${elist }" var="e">

				<tr>
					<td>${e.eiId }</td>
					<td>${e.eiName }</td>
					<td>${e.eiFileName }</td>
					<td>${e.eiTime }</td>
					<td>${e.eiSize }</td>
					<td>${e.eiCount }</td>
					<td colspan="4" style="text-align: center;"><a
						href="file/${e.eiName }"><button class="btn btn-primary">下载</button></a>&nbsp;&nbsp;<a
						href="javascript:if (confirm('确定要删除吗？')) location.href='deleteebook.action?id=${e.eiId }'"><button
								class="btn btn-danger">删除</button></a></td>
				</tr>
			</c:forEach>
			<!-- <tr>
				<td colspan="7" style="text-align: right;"><a href="upload.jsp"><button
							class="btn-success">上传图书</button></a></td>
			</tr>
			 -->
			<tr>
				<td colspan="10" style="text-align: center;"><a
					href="findAll.action?pageCode=1">首页</a> 第${pageCode}页 <a
					href="findAll.action?pageCode=${pageCode-1 }">上一页</a> <a
					href="findAll.action?pageCode=${pageCode+1 }">下一页</a> <a
					href="findAll.action?pageCode=${totalCode}">尾页</a> 到第<input
					type="text" name="page" id="page" size="2" />页 共${totalCode}页</td>
			</tr>
		</tbody>
	</table>
</body>
</html>