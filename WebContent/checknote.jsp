<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/bootstrap.js"></script>
</head>
<body>
	<div style="width: 800px; margin-left: 60px;">
			<div class="form-group" style="padding-top: 40px;">
				<label for="inputPassword" class="col-sm-2 control-label">姓名：</label>
				<div class="col-sm-10">
				<input type="hidden" value="${ni.noteId }"/>
					<input type="text" class="form-control" name="ni.noteAuthor"
					 disabled="disabled" value="${ni.noteAuthor }">
				</div>
			</div>
			<div class="form-group" style="padding-top: 40px;">
				<label for="inputPassword" class="col-sm-2 control-label">标题：</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="title1" name="ni.noteTitle"
						value="${ni.noteTitle }" disabled="disabled">
				</div>
			</div>
			<div class="form-group" style="padding-top: 40px;">
				<label for="inputPassword" class="col-sm-2 control-label">内容：</label>
				<div class="col-sm-10">
					<textarea class="form-control" id="content" name="ni.noteContent" disabled="disabled" rows="5px;">${ni.noteContent }</textarea>
				</div>
			</div>
			<div class="form-group" style="margin-top: 130px;">
				<label for="inputPassword" class="col-sm-2 control-label">状态：</label>
				<div class="col-sm-10">
					<c:if test="${ni.noteState eq 1 }"><input type="text" class="form-control"
						value="已审核" disabled="disabled"></c:if> 
						<c:if test="${ni.noteState eq 0 }"><input type="text" class="form-control"
						value="未审核" disabled="disabled"></c:if> 
				</div>
			</div>
			<div style="margin-left: 50%;padding-top: 40px;">
				<a href="checkagree.action?id=${ni.noteId }" class="btn btn-success">通过</a>
				<a href="javascript:window.history.back()" class="btn btn-default">返回</a>
			</div>
		</div>
</body>
</html>