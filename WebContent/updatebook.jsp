<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" />
</head>
<script type="text/javascript" src="js/jquery-1.8.3.js"></script>
<body>
	<form action="updatebyid" method="post">
		<table class="table table-bordered">
			<tr>
				<td style="text-align: center;">编号：</td>
				<td><input type="text" class="form-control" name="bif.bookId" value="${bi.bookId }" readonly="readonly" /></td>
			</tr>
			<tr>
				<td style="text-align: center;">书名：</td>
				<td><input type="text" class="form-control" name="bif.bookName" value="${bi.bookName }" /></td>
			</tr>
			<tr>
				<td style="text-align: center;">作者：</td>
				<td><input type="text" class="form-control" name="bif.bookAuthor" value="${bi.bookAuthor }" /></td>
			</tr>
			<tr>
				<td style="text-align: center;">ISBN：</td>
				<td><input type="text" class="form-control" name="bif.bookIsbn" value="${bi.bookIsbn }" /></td>
			</tr>
			<tr>
				<td style="text-align: center;">页数：</td>
				<td><input type="text" class="form-control" name="bif.bookPage" value="${bi.bookPage }" /></td>
			</tr>
			<tr>
				<td style="text-align: center;">字数：</td>
				<td><input type="text" class="form-control" name="bif.bookFontNumber" value="${bi.bookFontNumber }" /></td>
			</tr>
			<tr>
				<td style="text-align: center;">出版社：</td>
				<td><input type="text" class="form-control" name="bif.bookPublisher" value="${bi.bookPublisher }" /></td>
			</tr>
			<tr>
				<td style="text-align: center;">书的总数：</td>
				<td><input type="text" class="form-control" name="bif.bookNumAll" value="${bi.bookNumAll }" /></td>
			</tr>
			<tr align="center">
				<td colspan="2"><input type="submit" class="btn btn-success" value="修改" />&nbsp;&nbsp;&nbsp;<input type="reset" class="btn btn-danger" value="重填"  /></td>
			</tr>
		</table>
	</form>
</body>
</html>