<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" />
</head>
<script type="text/javascript" src="js/jquery-1.8.3.js"></script>
<script type="text/javascript" type="text/javascript">
	$(function() {
		$("#page").blur(function() {
			location.href = "findAllNote.action?pageCode=" + $("#page").val();
		});
	});
	$(function() {
		$("[id=find]").click(function() {
			var a = document.getElementById("name").value;
			if (a.length == 0) {
				alert("请输入关键字");
				return false;
			}
		});
	});
</script>
<body style="font-size: 15px; font-family: '楷体';">
	<table class="table table-bordered">
		<tr>
			<td colspan="7">
			<form action="findnotebyname" method="post">
			<input type="text" name="uname" placeholder="输入名字/部分名字"/>
			<input type="submit" value="查找" class="btn-default" />
			</form></td>
		</tr>
		<thead>
			<tr style="text-align: center;" class="success">
				<td>编号</td>
				<td>管理员</td>
				<td>标题</td>
				<td>内容</td>
				<td>留言时间</td>
				<td>状态</td>
				<td>操作</td>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td colspan="13">全部留言(根据留言时间排序)</td>
			</tr>
			<c:forEach items="${nlist }" var="n">

				<tr style="text-align: center;">
					<td>${n.noteId }</td>
					<td>${n.noteAuthor }</td>
					<td>${n.noteTitle }</td>
					<td>${fn:substring(n.noteContent,0,5) }···</td>
					<td>${n.noteTime }</td>
					<td><c:if test="${n.noteState eq 1 }">已审核</c:if>
						<c:if test="${n.noteState eq 0 }">未审核</c:if>
					 </td>
					<td style="text-align: center;">
					<c:if test="${n.noteState eq 0 }">
					<a
						href="checknote.action?noteId=${n.noteId }"><button
								class="btn btn-primary">审核留言</button></a></c:if>
								<c:if test="${n.noteState eq 1 }">
					<a href="#"><button
								class="btn btn-success" disabled="disabled">通过审核</button></a></c:if>
								
								&nbsp;&nbsp;<a
						href="javascript:if (confirm('确定要删除吗？')) location.href='deletenote.action?noteId=${n.noteId }'"><button
								class="btn btn-danger">删除留言</button></a></td>
				</tr>
			</c:forEach>
			<tr>
				<td colspan="13" style="text-align: center;"><a
					href="findAllNote.action?pageCode=1">首页</a>第${pageCode}页 <a
					href="findAllNote.action?pageCode=${pageCode-1 }">上一页</a> <a
					href="findAllNote.action?pageCode=${pageCode+1 }">下一页</a> <a
					href="findAllNote.action?pageCode=${totalCode}">尾页</a> 到第<input
					type="text" name="page" id="page" size="2" />页 共${totalCode}页</td>
			</tr>
		</tbody>
	</table>
</body>
</html>