<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" />
</head>
<body style="font-size: 15px; font-family: '楷体';">
	<table class="table table-bordered" style="text-align: center;">
		<tr>
			<td>编号</td>
			<td>登录名</td>
			<td>状态</td>
			<td>操作</td>
		</tr>
		<c:forEach items="${alist }" var="a">
			<tr>
				<td>${a.amiId }</td>
				<td>${a.amiName }</td>
				<td><c:if test="${a.amiState eq 0 }">正常</c:if> <c:if
						test="${a.amiState eq 1 }">锁定</c:if></td>
				<td><c:if test="${a.amiState eq 0 }">
						<a href="updatepwd.jsp?pwd=${a.amiPwd }&&id=${a.amiId }"
							class="btn btn-primary">修改密码</a>&nbsp;&nbsp;
						<a href="lock.action?id=${a.amiId }"><button
								class="btn btn-warning">锁定账号</button></a>
					</c:if> <c:if test="${a.amiState eq 1 }">
						<a href="updatepwd.jsp?pwd=${a.amiPwd }&&id=${a.amiId }"><button
								class="btn btn-primary" disabled="disabled">修改密码</button></a>
						<a
							href="javascript:if (confirm('确定要解除吗？')) location.href='unlock.action?id=${a.amiId }'"
							class="btn btn-warning">解除锁定</a>
					</c:if>&nbsp;&nbsp;<a
					href="javascript:if (confirm('删除账号会清空此账号下的所有留言，确定要删除吗？')) location.href='deleteadmin.action?id=${a.amiId }'"
					class="btn btn-danger">删除账号</a></td>
			</tr>
		</c:forEach>
	</table>
</body>
</html>