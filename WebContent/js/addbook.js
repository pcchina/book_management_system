$(function() {
	$("[id=add]").click(function() {
		var a = $("[id=name]").val();
		var b = $("[id=btype]").val();
		var c = $("[id=author]").val();
		var d = $("[id=isbn]").val();
		var e = $("[id=introduce]").val();
		var f = $("[id=pagecount]").val();
		var g = $("[id=fontcount]").val();
		var r = /^[0-9]*[1-9][0-9]*$/;
		var h = $("[id=publisher]").val();
		var i = $("[id=count]").val();
		if (a.length==0) {
			 alert("书名不能为空");
			return false;
		}else if(b=="0"){
			alert("请选择类型");
			return false;
		}else if(c.length==0){
			alert("请输入作者");
			return false;
		}else if(d.length==0){
			alert("请输入isbn");
			return false;
		}else if(e.length==0){
			alert("请输入简介");
			return false;
		}else if(e.length<=10){
			alert("简介不能少于10个字符");
			return false;
		}else if(e.length>=25){
			alert("简介不能大于25个字符");
			return false;
		}else if(f.length==0){
			alert("请输入页数");
			return false;
		}else if(isNaN(f)){
			alert("页数必须为数字");
			return false;
		}else if(!r.test(f)){
			alert("页数只能为正整数");
			return false;
		}else if(g.length==0){
			alert("请输入字数");
			return false;
		}else if(isNaN(g)){
			alert("字数必须为数字");
			return false;
		}else if(!r.test(g)){
			alert("字数只能为正整数");
			return false;
		}else if(h.length==0){
			alert("出版社不能为空");
			return false;
		}else if(isNaN(i)){
			alert("数量只能为数字");
			return false;
		}else if(!r.test(i)){
			alert("数量只能为正整数");
			return false;
		}
	});
});