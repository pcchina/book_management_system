<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
</head>
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" />
<body>
	<form action="updatepwd" method="post">
		<input type="hidden" name="id" value="${param.id }">
		<div class="form-group" style="width: 500px; margin: 100px auto;">
			<label class="control-label">旧密码：</label> <input type="text"
				class="form-control" value="${param.pwd }" readonly="readonly"><span></span>
			<label class="control-label">新密码：</label> <input type="text"
				class="form-control" name="pwd"><span></span>
				<div style="margin-left: 40%;">
				<input type="submit" class="btn btn-primary" value="确定" />
				<input type="reset" class="btn btn-default" value="重填" />
				</div>
		</div>
	</form>
</body>
</html>