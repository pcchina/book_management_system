﻿# Host: localhost  (Version: 5.0.18-nt)
# Date: 2017-10-23 17:13:57
# Generator: MySQL-Front 5.3  (Build 2.42)

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE */;
/*!40101 SET SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES */;
/*!40103 SET SQL_NOTES='ON' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS */;
/*!40014 SET FOREIGN_KEY_CHECKS=0 */;

DROP DATABASE IF EXISTS `bookmanager`;
CREATE DATABASE `bookmanager` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `bookmanager`;

#
# Source for table "adminmanager"
#

DROP TABLE IF EXISTS `adminmanager`;
CREATE TABLE `adminmanager` (
  `am_id` int(11) NOT NULL auto_increment,
  `am_name` varchar(20) NOT NULL,
  `am_password` varchar(20) NOT NULL,
  `am_state` int(11) default '0',
  PRIMARY KEY  (`am_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Data for table "adminmanager"
#

INSERT INTO `adminmanager` VALUES (1,'admin','123456',0),(2,'admin1','123456',0),(3,'admin2','123456',0),(4,'admin3','123456',1),(5,'admin4','123456',0),(6,'admin5','123456',0),(7,'admin6','123456',0),(8,'admin7','123456',0),(9,'admin8','123456',0);

#
# Source for table "booktype"
#

DROP TABLE IF EXISTS `booktype`;
CREATE TABLE `booktype` (
  `bt_id` int(11) NOT NULL auto_increment,
  `bt_name` varchar(30) default NULL,
  PRIMARY KEY  (`bt_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Data for table "booktype"
#

INSERT INTO `booktype` VALUES (1,'文学类'),(2,'历史类'),(3,'教育类'),(4,'幼儿类');

#
# Source for table "book"
#

DROP TABLE IF EXISTS `book`;
CREATE TABLE `book` (
  `b_id` int(11) NOT NULL auto_increment,
  `b_name` varchar(30) NOT NULL,
  `b_author` varchar(30) NOT NULL,
  `b_btid` int(11) default NULL,
  `b_isbn` varchar(30) default NULL,
  `b_introduce` text,
  `b_pages` int(11) default NULL,
  `b_fontnumber` varchar(30) default NULL,
  `b_publisher` varchar(40) default NULL,
  `b_intime` datetime default NULL,
  `b_newbooks` int(11) default '0',
  `b_bookstate` int(11) default '0',
  `b_booknum` int(11) default NULL,
  `b_booknumall` int(11) default '1',
  PRIMARY KEY  (`b_id`),
  KEY `b_btid` (`b_btid`),
  CONSTRAINT `book_ibfk_1` FOREIGN KEY (`b_btid`) REFERENCES `booktype` (`bt_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Data for table "book"
#

INSERT INTO `book` VALUES (1,'狂人日记','路人甲',1,'K12131','这是鲁迅的一部中长篇小说',2000,'50000','北京出版社','2017-10-11 00:00:00',1,1,10,30),(2,'狂人','鲁迅',1,'K12138','这是一本书',1000,'5000','北京出版社','2017-10-17 12:47:34',1,1,10,10),(3,'呐喊','鲁迅',1,'K12134','这是鲁迅的一本写实小说，巨好看，不看后悔一辈子',404,'4326754','北京出版社','2017-10-17 12:55:22',1,0,10,21),(4,'朝花夕拾1','大奔',3,'K12345','本书集合了世界上所有的可能与不可能的事情',404,'4326754','北京出版社','2017-10-17 13:02:52',1,1,5,25),(5,'朝花夕拾2','大奔',3,'K12345','本书集合了世界上所有的可能与不可能的事情',404,'4326754','北京出版社','2017-10-17 13:02:52',0,1,15,25),(6,'朝花夕拾3','大奔',3,'K12345','本书集合了世界上所有的可能与不可能的事情',404,'4326754','北京出版社','2017-10-17 13:02:52',0,1,15,25),(7,'朝花夕拾4','大奔',3,'K12345','本书集合了世界上所有的可能与不可能的事情',404,'4326754','北京出版社','2017-10-17 13:02:52',0,0,10,25),(8,'朝花夕拾5','大奔',3,'K12345','本书集合了世界上所有的可能与不可能的事情',404,'4326754','北京出版社','2017-10-17 13:02:52',0,0,12,25),(9,'朝花夕拾6','大奔',3,'K12345','本书集合了世界上所有的可能与不可能的事情',404,'4326754','北京出版社','2017-10-17 13:02:52',1,0,13,25),(10,'朝花夕拾7','大奔',3,'K12345','本书集合了世界上所有的可能与不可能的事情',404,'4326754','北京出版社','2017-10-17 13:02:52',1,0,14,25),(11,'朝花夕拾8','大奔',3,'K12345','本书集合了世界上所有的可能与不可能的事情',404,'4326754','北京出版社','2017-10-17 13:02:52',1,0,15,25),(12,'朝花夕拾9','大奔',3,'K12345','本书集合了世界上所有的可能与不可能的事情',404,'4326754','北京出版社','2017-10-17 13:02:52',0,0,11,25),(13,'朝花夕拾10','大奔',3,'K12345','本书集合了世界上所有的可能与不可能的事情',404,'4326754','北京出版社','2017-10-17 13:02:52',1,1,16,25),(14,'朝花夕拾11','大奔',3,'K12345','本书集合了世界上所有的可能与不可能的事情',404,'4326754','北京出版社','2017-10-17 13:02:52',0,0,17,25),(15,'朝花夕拾12','大奔',3,'K12345','本书集合了世界上所有的可能与不可能的事情',404,'4326754','北京出版社','2017-10-17 13:02:52',1,0,18,25),(16,'朝花夕拾13','大奔',3,'K12345','本书集合了世界上所有的可能与不可能的事情',404,'4326754','北京出版社','2017-10-17 13:02:52',1,1,25,25),(17,'朝花夕拾14','大奔',3,'K12345','本书集合了世界上所有的可能与不可能的事情',404,'4326754','北京出版社','2017-10-17 13:02:52',0,0,13,25),(18,'朝花夕拾15','大奔',3,'K12345','本书集合了世界上所有的可能与不可能的事情',404,'4326754','北京出版社','2017-10-17 13:02:52',1,0,14,25),(19,'钢铁是怎样炼成的1','诺夫拉丝机',1,'K13241','诺夫拉丝机的一本成名书，讲述了钢铁的制造过程',521,'54645111','大象出版社','2017-10-17 13:07:03',0,0,8,34),(20,'钢铁是怎样炼成的2','诺夫拉丝机',1,'K13242','诺夫拉丝机的一本成名书，讲述了钢铁的制造过程',521,'546451112','大象出版社','2017-10-17 13:07:03',1,0,7,34),(21,'钢铁是怎样炼成的3','诺夫拉丝机',1,'K13243','诺夫拉丝机的一本成名书，讲述了钢铁的制造过程',521,'5464511123','大象出版社','2017-10-17 13:07:03',1,1,6,34),(22,'钢铁是怎样炼成的4','诺夫拉丝机',1,'K13244','诺夫拉丝机的一本成名书，讲述了钢铁的制造过程',521,'54645111234','大象出版社','2017-10-17 13:07:03',0,0,5,34),(23,'钢铁是怎样炼成的5','诺夫拉丝机',1,'K13245','诺夫拉丝机的一本成名书，讲述了钢铁的制造过程',521,'546451112345','大象出版社','2017-10-17 13:07:03',1,1,4,34),(24,'钢铁是怎样炼成的6','诺夫拉丝机',1,'K13246','诺夫拉丝机的一本成名书，讲述了钢铁的制造过程',521,'5464511123','大象出版社','2017-10-17 13:07:03',1,0,12,34),(25,'钢铁是怎样炼成的7','诺夫拉丝机',1,'K13247','诺夫拉丝机的一本成名书，讲述了钢铁的制造过程',521,'5464511123','大象出版社','2017-10-17 13:07:03',0,1,3,34),(26,'钢铁是怎样炼成的8','诺夫拉丝机',1,'K13248','诺夫拉丝机的一本成名书，讲述了钢铁的制造过程',521,'5464511123','大象出版社','2017-10-17 13:07:03',1,0,15,34),(27,'钢铁是怎样炼成的9','诺夫拉丝机',1,'K13249','诺夫拉丝机的一本成名书，讲述了钢铁的制造过程',521,'5464511123','大象出版社','2017-10-17 13:07:03',1,0,12,34),(28,'钢铁是怎样炼成的10','诺夫拉丝机',1,'K132410','诺夫拉丝机的一本成名书，讲述了钢铁的制造过程',521,'5464511123','大象出版社','2017-10-17 13:07:03',1,1,17,34),(29,'钢铁是怎样炼成的11','诺夫拉丝机',1,'K132411','诺夫拉丝机的一本成名书，讲述了钢铁的制造过程',521,'5464511123','大象出版社','2017-10-17 13:07:03',0,0,19,34),(30,'钢铁是怎样炼成的12','诺夫拉丝机',1,'K132412','诺夫拉丝机的一本成名书，讲述了钢铁的制造过程',521,'5464511123','大象出版社','2017-10-17 13:07:03',1,1,16,34),(31,'钢铁是怎样炼成的13','诺夫拉丝机',1,'K132413','诺夫拉丝机的一本成名书，讲述了钢铁的制造过程',521,'5464511123','大象出版社','2017-10-17 13:07:03',0,0,13,34),(32,'钢铁是怎样炼成的14','诺夫拉丝机',1,'K132414','诺夫拉丝机的一本成名书，讲述了钢铁的制造过程',521,'5464511123','大象出版社','2017-10-17 13:07:03',1,0,11,34),(33,'钢铁是怎样炼成的15','诺夫拉丝机',1,'K132415','诺夫拉丝机的一本成名书，讲述了钢铁的制造过程',521,'5464511123','大象出版社','2017-10-17 13:07:03',0,0,15,34),(34,'大海捞针','酸辣',3,'L65478','讲的是孙悟空大闹天宫的故事',654,'4568974','新华出版社','2017-10-18 15:09:22',0,0,0,6),(35,'大闹天宫','吴承恩',1,'L123456','猪八戒，唐三藏，孙悟空',4561,'789465416','新华出版社','2017-10-18 15:14:00',0,0,0,10),(36,'错题本','慈吉中学',1,'I45621','全椒慈吉中学唯一出版，学科自选',400,'500006','北京出版社','2017-10-19 11:01:43',0,1,21,21);

#
# Source for table "ebook"
#

DROP TABLE IF EXISTS `ebook`;
CREATE TABLE `ebook` (
  `e_id` int(11) NOT NULL auto_increment,
  `e_name` varchar(50) default NULL,
  `e_filename` varchar(255) default NULL,
  `e_time` datetime default NULL,
  `e_count` int(11) default NULL,
  `e_size` varchar(1000) default NULL,
  PRIMARY KEY  (`e_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Data for table "ebook"
#

INSERT INTO `ebook` VALUES (1,'朝花夕拾.docx','1507872823385.docx','2017-10-13 13:33:43',580,'C:UsersAdministratorworkspace.metadata.pluginsorg.eclipse.wst.server.core\tmp0wtpwebappsBookManagerfile1507872823385.docx'),(2,'鲁迅文集.docx','1507872823418.docx','2017-10-13 13:33:43',320,'C:UsersAdministratorworkspace.metadata.pluginsorg.eclipse.wst.server.core\tmp0wtpwebappsBookManagerfile1507872823418.docx'),(3,'狂人日记.docx','1507872823431.docx','2017-10-13 13:33:43',530,'C:UsersAdministratorworkspace.metadata.pluginsorg.eclipse.wst.server.core\tmp0wtpwebappsBookManagerfile1507872823431.docx'),(4,'路遥.docx','1507872823460.docx','2017-10-13 13:33:43',520,'C:UsersAdministratorworkspace.metadata.pluginsorg.eclipse.wst.server.core\tmp0wtpwebappsBookManagerfile1507872823460.docx'),(5,'呐喊.docx','1507872852271.docx','2017-10-13 13:34:12',510,'C:UsersAdministratorworkspace.metadata.pluginsorg.eclipse.wst.server.core\tmp0wtpwebappsBookManagerfile1507872852271.docx'),(6,'彷徨.docx','1507872852312.docx','2017-10-13 13:34:12',460,'C:UsersAdministratorworkspace.metadata.pluginsorg.eclipse.wst.server.core\tmp0wtpwebappsBookManagerfile1507872852312.docx'),(7,'新青年.docx','1507872852362.docx','2017-10-13 13:34:12',460,'C:UsersAdministratorworkspace.metadata.pluginsorg.eclipse.wst.server.core\tmp0wtpwebappsBookManagerfile1507872852362.docx'),(8,'野草.docx','1507872852404.docx','2017-10-13 13:34:12',454,'C:UsersAdministratorworkspace.metadata.pluginsorg.eclipse.wst.server.core\tmp0wtpwebappsBookManagerfile1507872852404.docx'),(9,'中国小说史略.docx','1507872852443.docx','2017-10-13 13:34:12',324,'C:UsersAdministratorworkspace.metadata.pluginsorg.eclipse.wst.server.core\tmp0wtpwebappsBookManagerfile1507872852443.docx'),(10,'平凡的世界.docx','1507872938189.docx','2017-10-13 13:35:38',584,'C:UsersAdministratorworkspace.metadata.pluginsorg.eclipse.wst.server.core\tmp0wtpwebappsBookManagerfile1507872938189.docx'),(11,'平凡的世界2.docx','1507873009732.docx','2017-10-13 13:36:49',124,'C:UsersAdministratorworkspace.metadata.pluginsorg.eclipse.wst.server.core\tmp0wtpwebappsBookManagerfile1507873009732.docx'),(12,'平凡的世界3.docx','1507873009757.docx','2017-10-13 13:36:49',500,'C:UsersAdministratorworkspace.metadata.pluginsorg.eclipse.wst.server.core\tmp0wtpwebappsBookManagerfile1507873009757.docx'),(13,'平凡的世界4.docx','1507873009792.docx','2017-10-13 13:36:49',123,'C:UsersAdministratorworkspace.metadata.pluginsorg.eclipse.wst.server.core\tmp0wtpwebappsBookManagerfile1507873009792.docx'),(14,'平凡的世界5.docx','1507873009819.docx','2017-10-13 13:36:49',156,'C:UsersAdministratorworkspace.metadata.pluginsorg.eclipse.wst.server.core\tmp0wtpwebappsBookManagerfile1507873009819.docx'),(15,'平凡的世界6.docx','1507873009851.docx','2017-10-13 13:36:49',185,'C:UsersAdministratorworkspace.metadata.pluginsorg.eclipse.wst.server.core\tmp0wtpwebappsBookManagerfile1507873009851.docx'),(16,'平凡的世界7.docx','1507873041821.docx','2017-10-13 13:37:21',111,'C:UsersAdministratorworkspace.metadata.pluginsorg.eclipse.wst.server.core\tmp0wtpwebappsBookManagerfile1507873041821.docx'),(17,'平凡的世界8.docx','1507873041844.docx','2017-10-13 13:37:21',245,'C:UsersAdministratorworkspace.metadata.pluginsorg.eclipse.wst.server.core\tmp0wtpwebappsBookManagerfile1507873041844.docx'),(18,'平凡的世界9.docx','1507873041870.docx','2017-10-13 13:37:21',236,'C:UsersAdministratorworkspace.metadata.pluginsorg.eclipse.wst.server.core\tmp0wtpwebappsBookManagerfile1507873041870.docx'),(19,'平凡的世界10.docx','1507873041890.docx','2017-10-13 13:37:21',245,'C:UsersAdministratorworkspace.metadata.pluginsorg.eclipse.wst.server.core\tmp0wtpwebappsBookManagerfile1507873041890.docx'),(20,'平凡的世界11.docx','1507873070241.docx','2017-10-13 13:37:50',298,'C:UsersAdministratorworkspace.metadata.pluginsorg.eclipse.wst.server.core\tmp0wtpwebappsBookManagerfile1507873070241.docx'),(21,'平凡的世界12.docx','1507873070276.docx','2017-10-13 13:37:50',154,'C:UsersAdministratorworkspace.metadata.pluginsorg.eclipse.wst.server.core\tmp0wtpwebappsBookManagerfile1507873070276.docx'),(22,'平凡的世界13.docx','1507873070312.docx','2017-10-13 13:37:50',158,'C:UsersAdministratorworkspace.metadata.pluginsorg.eclipse.wst.server.core\tmp0wtpwebappsBookManagerfile1507873070312.docx'),(23,'平凡的世界14.docx','1507873070352.docx','2017-10-13 13:37:50',897,'C:UsersAdministratorworkspace.metadata.pluginsorg.eclipse.wst.server.core\tmp0wtpwebappsBookManagerfile1507873070352.docx'),(24,'平凡的世界15.docx','1507873105245.docx','2017-10-13 13:38:25',456,'C:UsersAdministratorworkspace.metadata.pluginsorg.eclipse.wst.server.core\tmp0wtpwebappsBookManagerfile1507873105245.docx'),(25,'平凡的世界16.docx','1507873105288.docx','2017-10-13 13:38:25',156,'C:UsersAdministratorworkspace.metadata.pluginsorg.eclipse.wst.server.core\tmp0wtpwebappsBookManagerfile1507873105288.docx'),(26,'平凡的世界17.docx','1507873105307.docx','2017-10-13 13:38:25',545,'C:UsersAdministratorworkspace.metadata.pluginsorg.eclipse.wst.server.core\tmp0wtpwebappsBookManagerfile1507873105307.docx'),(27,'平凡的世界18.docx','1507873105340.docx','2017-10-13 13:38:25',564,'C:UsersAdministratorworkspace.metadata.pluginsorg.eclipse.wst.server.core\tmp0wtpwebappsBookManagerfile1507873105340.docx'),(28,'平凡的世界19.docx','1507873133112.docx','2017-10-13 13:38:53',354,'C:UsersAdministratorworkspace.metadata.pluginsorg.eclipse.wst.server.core\tmp0wtpwebappsBookManagerfile1507873133112.docx'),(29,'平凡的世界20.docx','1507873133126.docx','2017-10-13 13:38:53',641,'C:UsersAdministratorworkspace.metadata.pluginsorg.eclipse.wst.server.core\tmp0wtpwebappsBookManagerfile1507873133126.docx'),(30,'平凡的世界21.docx','1507873133154.docx','2017-10-13 13:38:53',156,'C:UsersAdministratorworkspace.metadata.pluginsorg.eclipse.wst.server.core\tmp0wtpwebappsBookManagerfile1507873133154.docx'),(31,'平凡的世界22.docx','1507873133199.docx','2017-10-13 13:38:53',498,'C:UsersAdministratorworkspace.metadata.pluginsorg.eclipse.wst.server.core\tmp0wtpwebappsBookManagerfile1507873133199.docx'),(32,'平凡的世界23.docx','1507873133218.docx','2017-10-13 13:38:53',465,'C:UsersAdministratorworkspace.metadata.pluginsorg.eclipse.wst.server.core\tmp0wtpwebappsBookManagerfile1507873133218.docx'),(33,'平凡的世界24.docx','1507873166261.docx','2017-10-13 13:39:26',159,'C:UsersAdministratorworkspace.metadata.pluginsorg.eclipse.wst.server.core\tmp0wtpwebappsBookManagerfile1507873166261.docx'),(34,'平凡的世界25.docx','1507873166327.docx','2017-10-13 13:39:26',158,'C:UsersAdministratorworkspace.metadata.pluginsorg.eclipse.wst.server.core\tmp0wtpwebappsBookManagerfile1507873166327.docx'),(35,'平凡的世界26.docx','1507873166368.docx','2017-10-13 13:39:26',156,'C:UsersAdministratorworkspace.metadata.pluginsorg.eclipse.wst.server.core\tmp0wtpwebappsBookManagerfile1507873166368.docx'),(36,'平凡的世界27.docx','1507873166378.docx','2017-10-13 13:39:26',578,'C:UsersAdministratorworkspace.metadata.pluginsorg.eclipse.wst.server.core\tmp0wtpwebappsBookManagerfile1507873166378.docx'),(37,'朝花夕拾.docx','1508114810847.docx','2017-10-16 08:46:50',NULL,'C:UsersAdministratorworkspace.metadata.pluginsorg.eclipse.wst.server.core\tmp0wtpwebappsBookManagerfile1508114810847.docx'),(38,'鲁迅文集.docx','1508313015956.docx','2017-10-18 15:50:15',NULL,'C:UsersAdministratorworkspace.metadata.pluginsorg.eclipse.wst.server.core\tmp0wtpwebappsBookManagerfile1508313015956.docx');

#
# Source for table "note"
#

DROP TABLE IF EXISTS `note`;
CREATE TABLE `note` (
  `n_id` int(11) NOT NULL auto_increment,
  `am_id` int(11) default NULL,
  `n_title` varchar(20) NOT NULL,
  `n_author` varchar(20) NOT NULL,
  `n_content` text,
  `n_time` datetime default NULL,
  `n_state` int(11) default '0',
  PRIMARY KEY  (`n_id`),
  KEY `am_id` (`am_id`),
  CONSTRAINT `note_ibfk_1` FOREIGN KEY (`am_id`) REFERENCES `adminmanager` (`am_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Data for table "note"
#

INSERT INTO `note` VALUES (1,1,'辞职申请0','admin','由于将要毕业，所以决定辞去管理员一职，望通过0','2017-10-20 11:41:57',0),(2,1,'辞职申请1','admin','由于将要毕业，所以决定辞去管理员一职，望通过1','2017-10-20 11:41:57',0),(3,1,'辞职申请2','admin','由于将要毕业，所以决定辞去管理员一职，望通过2','2017-10-20 11:41:57',0),(4,1,'辞职申请3','admin','由于将要毕业，所以决定辞去管理员一职，望通过3','2017-10-20 11:41:57',0),(5,1,'辞职申请4','admin','由于将要毕业，所以决定辞去管理员一职，望通过4','2017-10-20 11:41:57',0),(6,1,'辞职申请5','admin','由于将要毕业，所以决定辞去管理员一职，望通过5','2017-10-20 11:41:57',0),(7,1,'辞职申请6','admin','由于将要毕业，所以决定辞去管理员一职，望通过6','2017-10-20 11:41:57',0),(8,1,'辞职申请7','admin','由于将要毕业，所以决定辞去管理员一职，望通过7','2017-10-20 11:41:57',0),(9,1,'辞职申请8','admin','由于将要毕业，所以决定辞去管理员一职，望通过8','2017-10-20 11:41:57',0),(10,1,'辞职申请9','admin','由于将要毕业，所以决定辞去管理员一职，望通过9','2017-10-20 11:41:57',0),(11,2,'辞职申请0','admin1','由于将要毕业，所以决定辞去管理员一职，望通过0','2017-10-20 11:44:43',0),(12,2,'辞职申请1','admin1','由于将要毕业，所以决定辞去管理员一职，望通过1','2017-10-20 11:44:43',0),(13,2,'辞职申请2','admin1','由于将要毕业，所以决定辞去管理员一职，望通过2','2017-10-20 11:44:43',0),(14,2,'辞职申请3','admin1','由于将要毕业，所以决定辞去管理员一职，望通过3','2017-10-20 11:44:43',0),(15,2,'辞职申请4','admin1','由于将要毕业，所以决定辞去管理员一职，望通过4','2017-10-20 11:44:43',0),(16,2,'辞职申请5','admin1','由于将要毕业，所以决定辞去管理员一职，望通过5','2017-10-20 11:44:43',0),(17,2,'辞职申请6','admin1','由于将要毕业，所以决定辞去管理员一职，望通过6','2017-10-20 11:44:43',0),(18,2,'辞职申请7','admin1','由于将要毕业，所以决定辞去管理员一职，望通过7','2017-10-20 11:44:43',0),(19,2,'辞职申请8','admin1','由于将要毕业，所以决定辞去管理员一职，望通过8','2017-10-20 11:44:43',0),(20,2,'辞职申请9','admin1','由于将要毕业，所以决定辞去管理员一职，望通过9','2017-10-20 11:44:43',0),(21,3,'辞职申请0','admin2','由于将要毕业，所以决定辞去管理员一职，望通过0','2017-10-20 11:45:22',0),(22,3,'辞职申请1','admin2','由于将要毕业，所以决定辞去管理员一职，望通过1','2017-10-20 11:45:22',0),(23,3,'辞职申请2','admin2','由于将要毕业，所以决定辞去管理员一职，望通过2','2017-10-20 11:45:22',0),(24,3,'辞职申请3','admin2','由于将要毕业，所以决定辞去管理员一职，望通过3','2017-10-20 11:45:22',0),(25,3,'辞职申请4','admin2','由于将要毕业，所以决定辞去管理员一职，望通过4','2017-10-20 11:45:22',0),(26,3,'辞职申请5','admin2','由于将要毕业，所以决定辞去管理员一职，望通过5','2017-10-20 11:45:22',0),(27,3,'辞职申请6','admin2','由于将要毕业，所以决定辞去管理员一职，望通过6','2017-10-20 11:45:22',0),(28,3,'辞职申请7','admin2','由于将要毕业，所以决定辞去管理员一职，望通过7','2017-10-20 11:45:22',0),(29,3,'辞职申请8','admin2','由于将要毕业，所以决定辞去管理员一职，望通过8','2017-10-20 11:45:22',0),(30,3,'辞职申请9','admin2','由于将要毕业，所以决定辞去管理员一职，望通过9','2017-10-20 11:45:22',0),(31,4,'辞职申请0','admin3','由于将要毕业，所以决定辞去管理员一职，望通过0','2017-10-20 11:46:11',0),(32,4,'辞职申请1','admin3','由于将要毕业，所以决定辞去管理员一职，望通过1','2017-10-20 11:46:11',0),(33,4,'辞职申请2','admin3','由于将要毕业，所以决定辞去管理员一职，望通过2','2017-10-20 11:46:11',0),(34,4,'辞职申请3','admin3','由于将要毕业，所以决定辞去管理员一职，望通过3','2017-10-20 11:46:11',0),(35,4,'辞职申请4','admin3','由于将要毕业，所以决定辞去管理员一职，望通过4','2017-10-20 11:46:11',0),(36,4,'辞职申请5','admin3','由于将要毕业，所以决定辞去管理员一职，望通过5','2017-10-20 11:46:11',0),(37,4,'辞职申请6','admin3','由于将要毕业，所以决定辞去管理员一职，望通过6','2017-10-20 11:46:11',0),(38,4,'辞职申请7','admin3','由于将要毕业，所以决定辞去管理员一职，望通过7','2017-10-20 11:46:11',0),(39,4,'辞职申请8','admin3','由于将要毕业，所以决定辞去管理员一职，望通过8','2017-10-20 11:46:11',0),(40,4,'辞职申请9','admin3','由于将要毕业，所以决定辞去管理员一职，望通过9','2017-10-20 11:46:11',0),(41,5,'辞职申请0','admin4','由于将要毕业，所以决定辞去管理员一职，望通过0','2017-10-20 11:46:35',0),(42,5,'辞职申请1','admin4','由于将要毕业，所以决定辞去管理员一职，望通过1','2017-10-20 11:46:35',0),(43,5,'辞职申请2','admin4','由于将要毕业，所以决定辞去管理员一职，望通过2','2017-10-20 11:46:35',0),(44,5,'辞职申请3','admin4','由于将要毕业，所以决定辞去管理员一职，望通过3','2017-10-20 11:46:36',0),(45,5,'辞职申请4','admin4','由于将要毕业，所以决定辞去管理员一职，望通过4','2017-10-20 11:46:36',0),(46,5,'辞职申请5','admin4','由于将要毕业，所以决定辞去管理员一职，望通过5','2017-10-20 11:46:36',0),(47,5,'辞职申请6','admin4','由于将要毕业，所以决定辞去管理员一职，望通过6','2017-10-20 11:46:36',0),(48,5,'辞职申请7','admin4','由于将要毕业，所以决定辞去管理员一职，望通过7','2017-10-20 11:46:36',0),(49,5,'辞职申请8','admin4','由于将要毕业，所以决定辞去管理员一职，望通过8','2017-10-20 11:46:36',0),(50,5,'辞职申请9','admin4','由于将要毕业，所以决定辞去管理员一职，望通过9','2017-10-20 11:46:36',0),(51,6,'辞职申请0','admin5','由于将要毕业，所以决定辞去管理员一职，望通过0','2017-10-20 11:47:09',0),(52,6,'辞职申请1','admin5','由于将要毕业，所以决定辞去管理员一职，望通过1','2017-10-20 11:47:09',0),(53,6,'辞职申请2','admin5','由于将要毕业，所以决定辞去管理员一职，望通过2','2017-10-20 11:47:09',0),(54,6,'辞职申请3','admin5','由于将要毕业，所以决定辞去管理员一职，望通过3','2017-10-20 11:47:09',0),(55,6,'辞职申请4','admin5','由于将要毕业，所以决定辞去管理员一职，望通过4','2017-10-20 11:47:09',0),(56,6,'辞职申请5','admin5','由于将要毕业，所以决定辞去管理员一职，望通过5','2017-10-20 11:47:09',0),(57,6,'辞职申请6','admin5','由于将要毕业，所以决定辞去管理员一职，望通过6','2017-10-20 11:47:09',0),(58,6,'辞职申请7','admin5','由于将要毕业，所以决定辞去管理员一职，望通过7','2017-10-20 11:47:09',0),(59,6,'辞职申请8','admin5','由于将要毕业，所以决定辞去管理员一职，望通过8','2017-10-20 11:47:09',0),(60,6,'辞职申请9','admin5','由于将要毕业，所以决定辞去管理员一职，望通过9','2017-10-20 11:47:09',0),(61,7,'辞职申请0','admin6','由于将要毕业，所以决定辞去管理员一职，望通过0','2017-10-20 11:47:32',0),(62,7,'辞职申请1','admin6','由于将要毕业，所以决定辞去管理员一职，望通过1','2017-10-20 11:47:32',0),(63,7,'辞职申请2','admin6','由于将要毕业，所以决定辞去管理员一职，望通过2','2017-10-20 11:47:32',0),(64,7,'辞职申请3','admin6','由于将要毕业，所以决定辞去管理员一职，望通过3','2017-10-20 11:47:32',0),(65,7,'辞职申请4','admin6','由于将要毕业，所以决定辞去管理员一职，望通过4','2017-10-20 11:47:32',0),(66,7,'辞职申请5','admin6','由于将要毕业，所以决定辞去管理员一职，望通过5','2017-10-20 11:47:32',0),(67,7,'辞职申请6','admin6','由于将要毕业，所以决定辞去管理员一职，望通过6','2017-10-20 11:47:32',0),(68,7,'辞职申请7','admin6','由于将要毕业，所以决定辞去管理员一职，望通过7','2017-10-20 11:47:32',0),(69,7,'辞职申请8','admin6','由于将要毕业，所以决定辞去管理员一职，望通过8','2017-10-20 11:47:32',0),(70,7,'辞职申请9','admin6','由于将要毕业，所以决定辞去管理员一职，望通过9','2017-10-20 11:47:32',0),(71,8,'辞职申请0','admin7','由于将要毕业，所以决定辞去管理员一职，望通过0','2017-10-20 11:48:21',0),(72,8,'辞职申请1','admin7','由于将要毕业，所以决定辞去管理员一职，望通过1','2017-10-20 11:48:22',0),(73,8,'辞职申请2','admin7','由于将要毕业，所以决定辞去管理员一职，望通过2','2017-10-20 11:48:22',1),(74,8,'辞职申请3','admin7','由于将要毕业，所以决定辞去管理员一职，望通过3','2017-10-20 11:48:22',0),(75,8,'辞职申请4','admin7','由于将要毕业，所以决定辞去管理员一职，望通过4','2017-10-20 11:48:22',0),(76,8,'辞职申请5','admin7','由于将要毕业，所以决定辞去管理员一职，望通过5','2017-10-20 11:48:22',1),(77,8,'辞职申请6','admin7','由于将要毕业，所以决定辞去管理员一职，望通过6','2017-10-20 11:48:22',0),(78,8,'辞职申请7','admin7','由于将要毕业，所以决定辞去管理员一职，望通过7','2017-10-20 11:48:22',0),(79,8,'辞职申请8','admin7','由于将要毕业，所以决定辞去管理员一职，望通过8','2017-10-20 11:48:22',0),(80,8,'辞职申请9','admin7','由于将要毕业，所以决定辞去管理员一职，望通过9','2017-10-20 11:48:22',0),(81,9,'辞职申请0','admin8','由于将要毕业，所以决定辞去管理员一职，望通过0','2017-10-20 11:48:56',0),(82,9,'辞职申请1','admin8','由于将要毕业，所以决定辞去管理员一职，望通过1','2017-10-20 11:48:56',0),(83,9,'辞职申请2','admin8','由于将要毕业，所以决定辞去管理员一职，望通过2','2017-10-20 11:48:56',0),(84,9,'辞职申请3','admin8','由于将要毕业，所以决定辞去管理员一职，望通过3','2017-10-20 11:48:56',1),(85,9,'辞职申请4','admin8','由于将要毕业，所以决定辞去管理员一职，望通过4','2017-10-20 11:48:56',1),(86,9,'辞职申请5','admin8','由于将要毕业，所以决定辞去管理员一职，望通过5','2017-10-20 11:48:56',0),(87,9,'辞职申请6','admin8','由于将要毕业，所以决定辞去管理员一职，望通过6','2017-10-20 11:48:56',0),(88,9,'辞职申请7','admin8','由于将要毕业，所以决定辞去管理员一职，望通过7','2017-10-20 11:48:56',0),(89,9,'辞职申请8','admin8','由于将要毕业，所以决定辞去管理员一职，望通过8','2017-10-20 11:48:56',0),(90,9,'辞职申请9','admin8','由于将要毕业，所以决定辞去管理员一职，望通过9','2017-10-20 11:48:56',1);

#
# Source for table "supermanager"
#

DROP TABLE IF EXISTS `supermanager`;
CREATE TABLE `supermanager` (
  `sm_id` int(11) NOT NULL auto_increment,
  `sm_name` varchar(20) NOT NULL,
  `sm_password` varchar(20) NOT NULL,
  PRIMARY KEY  (`sm_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Data for table "supermanager"
#

INSERT INTO `supermanager` VALUES (1,'llh','111111');

/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
